# IrisTK

This is the repository for IrisTK - a Java-based framework for developing multi-modal interactive systems.

Unlike the distribution on the IrisTK website, the code here does not contain any Java binaries, so it must first be compiled (in Eclipse). To do so, open a command prompt and go to the IrisTK folder, from there run "iristk install". Then you can import the project into Eclipse, and it will be compiled.

To read more about IrisTK, please visit [www.iristk.net](http://www.iristk.net)

####### README appended to by Alice Harris, 21/03/2019 #######

To set up the classpath for Eclipse importing, once "iristk install" has been run, run the command "iristk classpath". This will add a .classpath file in the root of the IrisTK folder.

Then, open Eclipse (or re-open if it was open before installation), and choose File > Import > Existing Projects into Workspace and select the IrisTK folder as the root directory.

The dialogue system should already be compiled, and runnable by navigating in the Project Explorer to GraceSystem.java (apps/grace/src > iristk.app.grace > GraceSystem.java), right clicking and selecting "Run as Java Application".

In case you wish to recompile the GraceFlow.xml dialogue flow, there is an Eclipse plugin. For details on its installation, and further guidance on the instructions above, please see [iristk.net/develop_in_eclipse](http://www.iristk.net/develop_in_eclipse.html)
