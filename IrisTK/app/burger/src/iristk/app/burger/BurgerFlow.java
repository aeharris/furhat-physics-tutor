package iristk.app.burger;

import java.util.List;
import java.io.File;
import iristk.xml.XmlMarshaller.XMLLocation;
import iristk.system.Event;
import iristk.flow.*;
import iristk.util.Record;
import static iristk.util.Converters.*;
import static iristk.flow.State.*;

public class BurgerFlow extends iristk.flow.Flow {

	private Record order;

	private void initVariables() {
		order = asRecord(new Record());
	}

	public Record getOrder() {
		return this.order;
	}

	public void setOrder(Record value) {
		this.order = value;
	}

	@Override
	public Object getVariable(String name) {
		if (name.equals("order")) return this.order;
		return null;
	}


	public BurgerFlow() {
		initVariables();
	}

	@Override
	public State getInitialState() {return new Start();}


	public class Start extends Dialog implements Initial {

		final State currentState = this;


		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 12
			try {
				EXECUTION: {
					int count = getCount(1334729950) + 1;
					incrCount(1334729950);
					// Line: 13
					if (count == 1) {
						iristk.flow.DialogFlow.say state0 = new iristk.flow.DialogFlow.say();
						StringCreator string1 = new StringCreator();
						string1.append("Welcome");
						state0.setText(string1.toString());
						if (!flowThread.callState(state0, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 13, 26)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					iristk.flow.DialogFlow.say state2 = new iristk.flow.DialogFlow.say();
					StringCreator string3 = new StringCreator();
					string3.append("May I please take your order");
					state2.setText(string3.toString());
					if (!flowThread.callState(state2, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 12, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					iristk.flow.DialogFlow.listen state4 = new iristk.flow.DialogFlow.listen();
					if (!flowThread.callState(state4, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 12, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 12, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class Dialog extends State {

		final State currentState = this;


		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 22
			try {
				count = getCount(1212899836) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:order")) {
						incrCount(1212899836);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 23
							order.adjoin(asRecord(event.get("sem:order")));
							// Line: 24
							CheckOrder state5 = new CheckOrder();
							flowThread.gotoState(state5, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 24, 31)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 22, 60));
			}
			// Line: 26
			try {
				count = getCount(1285044316) + 1;
				if (event.triggers("sense.user.speak")) {
					incrCount(1285044316);
					eventResult = EVENT_CONSUMED;
					EXECUTION: {
						iristk.flow.DialogFlow.say state6 = new iristk.flow.DialogFlow.say();
						StringCreator string7 = new StringCreator();
						string7.append("Sorry, I");
						// Line: 26
						if (count > 1) {
							string7.append("still");
						}
						string7.append("didn't get that.");
						state6.setText(string7.toString());
						if (!flowThread.callState(state6, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 26, 36)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						// Line: 30
						flowThread.reentryState(this, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 30, 15)));
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					if (eventResult != EVENT_IGNORED) return eventResult;
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 26, 36));
			}
			// Line: 32
			try {
				count = getCount(1811075214) + 1;
				if (event.triggers("sense.user.silence")) {
					incrCount(1811075214);
					eventResult = EVENT_CONSUMED;
					EXECUTION: {
						iristk.flow.DialogFlow.say state8 = new iristk.flow.DialogFlow.say();
						StringCreator string9 = new StringCreator();
						string9.append("Sorry, I");
						// Line: 32
						if (count > 1) {
							string9.append("still");
						}
						string9.append("didn't hear anything.");
						state8.setText(string9.toString());
						if (!flowThread.callState(state8, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 32, 38)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						// Line: 36
						flowThread.reentryState(this, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 36, 15)));
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					if (eventResult != EVENT_IGNORED) return eventResult;
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 32, 38));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class CheckOrder extends State {

		final State currentState = this;


		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 41
			try {
				EXECUTION: {
					int count = getCount(1940447180) + 1;
					incrCount(1940447180);
					// Line: 42
					if (!order.has("main")) {
						// Line: 43
						RequestMain state10 = new RequestMain();
						flowThread.gotoState(state10, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 43, 33)));
						eventResult = EVENT_ABORTED;
						break EXECUTION;
						// Line: 44
					} else if (!order.has("drink")) {
						// Line: 45
						RequestDrink state11 = new RequestDrink();
						flowThread.gotoState(state11, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 45, 34)));
						eventResult = EVENT_ABORTED;
						break EXECUTION;
						// Line: 47
					} else if (eq(order.get("drink:type"), "milkshake") && !order.has("drink:flavor")) {
						// Line: 48
						RequestFlavor state12 = new RequestFlavor();
						flowThread.gotoState(state12, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 48, 35)));
						eventResult = EVENT_ABORTED;
						break EXECUTION;
						// Line: 49
					} else if (!order.has("side")) {
						// Line: 50
						RequestSide state13 = new RequestSide();
						flowThread.gotoState(state13, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 50, 33)));
						eventResult = EVENT_ABORTED;
						break EXECUTION;
						// Line: 51
					} else {
						// Line: 52
						Done state14 = new Done();
						flowThread.gotoState(state14, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 52, 26)));
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 41, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class RequestMain extends Dialog {

		final State currentState = this;


		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 58
			try {
				EXECUTION: {
					int count = getCount(517938326) + 1;
					incrCount(517938326);
					iristk.flow.DialogFlow.say state15 = new iristk.flow.DialogFlow.say();
					StringCreator string16 = new StringCreator();
					string16.append("Do you want a hamburger?");
					state15.setText(string16.toString());
					if (!flowThread.callState(state15, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 58, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					iristk.flow.DialogFlow.listen state17 = new iristk.flow.DialogFlow.listen();
					if (!flowThread.callState(state17, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 58, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 58, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 62
			try {
				count = getCount(914424520) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:yes")) {
						incrCount(914424520);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 63
							order.putIfNotNull("main:type", "hamburger");
							// Line: 64
							CheckOrder state18 = new CheckOrder();
							flowThread.gotoState(state18, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 64, 31)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 62, 58));
			}
			// Line: 66
			try {
				count = getCount(2143192188) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:no")) {
						incrCount(2143192188);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 67
							order.putIfNotNull("main:type", "none");
							// Line: 68
							CheckOrder state19 = new CheckOrder();
							flowThread.gotoState(state19, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 68, 31)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 66, 57));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class RequestDrink extends Dialog {

		final State currentState = this;


		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 73
			try {
				EXECUTION: {
					int count = getCount(2110121908) + 1;
					incrCount(2110121908);
					iristk.flow.DialogFlow.say state20 = new iristk.flow.DialogFlow.say();
					StringCreator string21 = new StringCreator();
					string21.append("Do you want anything to drink?");
					state20.setText(string21.toString());
					if (!flowThread.callState(state20, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 73, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					iristk.flow.DialogFlow.listen state22 = new iristk.flow.DialogFlow.listen();
					if (!flowThread.callState(state22, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 73, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 73, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 77
			try {
				count = getCount(32374789) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:yes")) {
						incrCount(32374789);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							iristk.flow.DialogFlow.say state23 = new iristk.flow.DialogFlow.say();
							StringCreator string24 = new StringCreator();
							string24.append("So what do you want to drink?");
							state23.setText(string24.toString());
							if (!flowThread.callState(state23, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 77, 58)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							iristk.flow.DialogFlow.listen state25 = new iristk.flow.DialogFlow.listen();
							if (!flowThread.callState(state25, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 77, 58)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 77, 58));
			}
			// Line: 81
			try {
				count = getCount(1973538135) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:no")) {
						incrCount(1973538135);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 82
							order.putIfNotNull("drink:type", "none");
							// Line: 83
							CheckOrder state26 = new CheckOrder();
							flowThread.gotoState(state26, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 83, 31)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 81, 57));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class RequestFlavor extends Dialog {

		final State currentState = this;


		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 88
			try {
				EXECUTION: {
					int count = getCount(1694819250) + 1;
					incrCount(1694819250);
					iristk.flow.DialogFlow.say state27 = new iristk.flow.DialogFlow.say();
					StringCreator string28 = new StringCreator();
					string28.append("What flavor do you want in your milkshake?");
					state27.setText(string28.toString());
					if (!flowThread.callState(state27, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 88, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					iristk.flow.DialogFlow.listen state29 = new iristk.flow.DialogFlow.listen();
					if (!flowThread.callState(state29, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 88, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 88, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 92
			try {
				count = getCount(1365202186) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:flavor")) {
						incrCount(1365202186);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 93
							order.putIfNotNull("drink:flavor", event.get("sem:flavor"));
							// Line: 94
							CheckOrder state30 = new CheckOrder();
							flowThread.gotoState(state30, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 94, 31)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 92, 61));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class RequestSide extends Dialog {

		final State currentState = this;


		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 99
			try {
				EXECUTION: {
					int count = getCount(932583850) + 1;
					incrCount(932583850);
					iristk.flow.DialogFlow.say state31 = new iristk.flow.DialogFlow.say();
					StringCreator string32 = new StringCreator();
					string32.append("Do you want anything on the side, such as fries or 				salad?");
					state31.setText(string32.toString());
					if (!flowThread.callState(state31, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 99, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					iristk.flow.DialogFlow.listen state33 = new iristk.flow.DialogFlow.listen();
					if (!flowThread.callState(state33, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 99, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 99, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 104
			try {
				count = getCount(212628335) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:yes")) {
						incrCount(212628335);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							iristk.flow.DialogFlow.say state34 = new iristk.flow.DialogFlow.say();
							StringCreator string35 = new StringCreator();
							string35.append("So what do you want on the side?");
							state34.setText(string35.toString());
							if (!flowThread.callState(state34, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 104, 58)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							iristk.flow.DialogFlow.listen state36 = new iristk.flow.DialogFlow.listen();
							if (!flowThread.callState(state36, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 104, 58)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 104, 58));
			}
			// Line: 108
			try {
				count = getCount(359023572) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:no")) {
						incrCount(359023572);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 109
							order.putIfNotNull("side:type", "none");
							// Line: 110
							CheckOrder state37 = new CheckOrder();
							flowThread.gotoState(state37, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 110, 31)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 108, 57));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class Done extends State {

		final State currentState = this;


		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 115
			try {
				EXECUTION: {
					int count = getCount(917142466) + 1;
					incrCount(917142466);
					iristk.flow.DialogFlow.say state38 = new iristk.flow.DialogFlow.say();
					StringCreator string39 = new StringCreator();
					string39.append("Okay, thanks for your order");
					state38.setText(string39.toString());
					if (!flowThread.callState(state38, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 115, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					// Line: 117
					StringCreator string40 = new StringCreator();
					// Line: 118
					string40.append(order.toStringIndent());
					log(string40.toString());
					// Line: 120
					System.exit(0);
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\burger\\src\\iristk\\app\\burger\\BurgerFlow.xml"), 115, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


}
