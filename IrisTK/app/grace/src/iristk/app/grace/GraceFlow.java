package iristk.app.grace;

import java.util.List;
import java.io.File;
import iristk.xml.XmlMarshaller.XMLLocation;
import iristk.system.Event;
import iristk.flow.*;
import iristk.util.Record;
import static iristk.util.Converters.*;
import static iristk.flow.State.*;

public class GraceFlow extends iristk.flow.Flow {

	private iristk.situated.SystemAgentFlow agent;
	private iristk.situated.SystemAgent system;

	private void initVariables() {
		system = (iristk.situated.SystemAgent) agent.getSystemAgent();
	}

	public iristk.situated.SystemAgent getSystem() {
		return this.system;
	}

	public void setSystem(iristk.situated.SystemAgent value) {
		this.system = value;
	}

	public iristk.situated.SystemAgentFlow getAgent() {
		return this.agent;
	}

	@Override
	public Object getVariable(String name) {
		if (name.equals("system")) return this.system;
		if (name.equals("agent")) return this.agent;
		return null;
	}


	public GraceFlow(iristk.situated.SystemAgentFlow agent) {
		this.agent = agent;
		initVariables();
	}

	@Override
	public State getInitialState() {return new Idle();}


	public class Idle extends State implements Initial {

		final State currentState = this;


		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 13
			try {
				EXECUTION: {
					int count = getCount(1347137144) + 1;
					incrCount(1347137144);
					// Line: 14
					if (system.hasUsers()) {
						iristk.situated.SystemAgentFlow.attendRandom state0 = agent.new attendRandom();
						if (!flowThread.callState(state0, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 14, 33)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						// Line: 16
						Greeting state1 = new Greeting();
						flowThread.gotoState(state1, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 16, 29)));
						eventResult = EVENT_ABORTED;
						break EXECUTION;
						// Line: 17
					} else {
						iristk.situated.SystemAgentFlow.attendNobody state2 = agent.new attendNobody();
						if (!flowThread.callState(state2, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 14, 33)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 13, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 21
			try {
				count = getCount(1289696681) + 1;
				if (event.triggers("sense.user.enter")) {
					incrCount(1289696681);
					eventResult = EVENT_CONSUMED;
					EXECUTION: {
						iristk.situated.SystemAgentFlow.attend state3 = agent.new attend();
						state3.setTarget(event.get("user"));
						if (!flowThread.callState(state3, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 21, 36)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						// Line: 23
						Greeting state4 = new Greeting();
						flowThread.gotoState(state4, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 23, 28)));
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					if (eventResult != EVENT_IGNORED) return eventResult;
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 21, 36));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class Dialog extends State {

		final State currentState = this;


		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 28
			try {
				count = getCount(1811075214) + 1;
				if (event.triggers("sense.user.leave")) {
					if (system.isAttending(event)) {
						incrCount(1811075214);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 29
							if (system.hasUsers()) {
								iristk.situated.SystemAgentFlow.attendRandom state5 = agent.new attendRandom();
								if (!flowThread.callState(state5, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 29, 33)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
								// Line: 31
								flowThread.reentryState(this, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 31, 15)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
								// Line: 32
							} else {
								// Line: 33
								Goodbye state6 = new Goodbye();
								flowThread.gotoState(state6, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 33, 28)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 28, 70));
			}
			// Line: 36
			try {
				count = getCount(2121744517) + 1;
				if (event.triggers("sense.user.speech.start")) {
					if (system.isAttending(event) && eq(event.get("speakers"), 1)) {
						incrCount(2121744517);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							iristk.situated.SystemAgentFlow.gesture state7 = agent.new gesture();
							state7.setName("smile");
							if (!flowThread.callState(state7, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 36, 102)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 36, 102));
			}
			// Line: 39
			try {
				count = getCount(1066376662) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:leaving")) {
						incrCount(1066376662);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 40
							ExitDialog state8 = new ExitDialog();
							if (!flowThread.callState(state8, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 40, 31)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 41
							flowThread.reentryState(this, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 41, 15)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 39, 62));
			}
			// Line: 43
			try {
				count = getCount(1490180672) + 1;
				if (event.triggers("sense.user.speak")) {
					incrCount(1490180672);
					eventResult = EVENT_CONSUMED;
					EXECUTION: {
						// Line: 44
						boolean chosen9 = false;
						boolean matching10 = true;
						while (!chosen9 && matching10) {
							int rand11 = random(460332449, 2, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
							matching10 = false;
							if (true) {
								matching10 = true;
								if (rand11 >= 0 && rand11 < 1) {
									chosen9 = true;
									iristk.situated.SystemAgentFlow.say state12 = agent.new say();
									StringCreator string13 = new StringCreator();
									string13.append("Sorry, I");
									// Line: 44
									if (count > 1) {
										string13.append("still");
									}
									string13.append("didn't get that.");
									state12.setText(string13.toString());
									if (!flowThread.callState(state12, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 44, 12)))) {
										eventResult = EVENT_ABORTED;
										break EXECUTION;
									}
								}
							}
							if (true) {
								matching10 = true;
								if (rand11 >= 1 && rand11 < 2) {
									chosen9 = true;
									iristk.situated.SystemAgentFlow.say state14 = agent.new say();
									StringCreator string15 = new StringCreator();
									string15.append("Apologies, I");
									// Line: 44
									if (count > 1) {
										string15.append("still");
									}
									string15.append("couldn't quite  				understand you.");
									state14.setText(string15.toString());
									if (!flowThread.callState(state14, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 44, 12)))) {
										eventResult = EVENT_ABORTED;
										break EXECUTION;
									}
								}
							}
						}
						// Line: 49
						flowThread.reentryState(this, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 49, 15)));
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					if (eventResult != EVENT_IGNORED) return eventResult;
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 43, 36));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class Greeting extends Dialog {

		final State currentState = this;


		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 54
			try {
				EXECUTION: {
					int count = getCount(250075633) + 1;
					incrCount(250075633);
					iristk.situated.SystemAgentFlow.say state16 = agent.new say();
					StringCreator string17 = new StringCreator();
					string17.append("Hello! My name is Grace; I'm a social robot and I've been  				programmed with technical wizardry to help teach people things! At the  				moment I can help out with some newtonian mechanics! When you pick a 				topic, I'll have a question or two that should explain the relevant concepts.");
					state16.setText(string17.toString());
					if (!flowThread.callState(state16, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 54, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					// Line: 59
					Start state18 = new Start();
					flowThread.gotoState(state18, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 59, 25)));
					eventResult = EVENT_ABORTED;
					break EXECUTION;
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 54, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class Start extends Dialog {

		final State currentState = this;


		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 65
			try {
				EXECUTION: {
					int count = getCount(110718392) + 1;
					incrCount(110718392);
					// Line: 66
					boolean chosen19 = false;
					boolean matching20 = true;
					while (!chosen19 && matching20) {
						int rand21 = random(425918570, 3, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
						matching20 = false;
						if (true) {
							matching20 = true;
							if (rand21 >= 0 && rand21 < 1) {
								chosen19 = true;
								iristk.situated.SystemAgentFlow.say state22 = agent.new say();
								StringCreator string23 = new StringCreator();
								string23.append("Would you like the list of topics I can help with.");
								state22.setText(string23.toString());
								if (!flowThread.callState(state22, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 66, 12)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
							}
						}
						if (true) {
							matching20 = true;
							if (rand21 >= 1 && rand21 < 2) {
								chosen19 = true;
								iristk.situated.SystemAgentFlow.say state24 = agent.new say();
								StringCreator string25 = new StringCreator();
								string25.append("Do you want to hear the topics I can help with.");
								state24.setText(string25.toString());
								if (!flowThread.callState(state24, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 66, 12)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
							}
						}
						if (true) {
							matching20 = true;
							if (rand21 >= 2 && rand21 < 3) {
								chosen19 = true;
								iristk.situated.SystemAgentFlow.say state26 = agent.new say();
								StringCreator string27 = new StringCreator();
								string27.append("Shall I tell you the list of topics I can help with.");
								state26.setText(string27.toString());
								if (!flowThread.callState(state26, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 66, 12)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
							}
						}
					}
					iristk.situated.SystemAgentFlow.listen state28 = agent.new listen();
					if (!flowThread.callState(state28, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 65, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 65, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 73
			try {
				count = getCount(2143192188) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:yes")) {
						incrCount(2143192188);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							iristk.situated.SystemAgentFlow.say state29 = agent.new say();
							StringCreator string30 = new StringCreator();
							string30.append("All right!");
							state29.setText(string30.toString());
							if (!flowThread.callState(state29, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 73, 58)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 75
							TopicList state31 = new TopicList();
							flowThread.gotoState(state31, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 75, 30)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 73, 58));
			}
			// Line: 77
			try {
				count = getCount(204349222) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:no")) {
						incrCount(204349222);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 78
							ExitDialog state32 = new ExitDialog();
							if (!flowThread.callState(state32, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 78, 31)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 79
							flowThread.reentryState(this, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 79, 15)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 77, 57));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class TopicList extends Dialog {

		final State currentState = this;

		public Object topicChoice;
		public Object repeat;

		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 86
			try {
				EXECUTION: {
					int count = getCount(1023487453) + 1;
					incrCount(1023487453);
					// Line: 87
					if (count == 1) {
						// Line: 88
						boolean chosen33 = false;
						boolean matching34 = true;
						while (!chosen33 && matching34) {
							int rand35 = random(515132998, 2, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
							matching34 = false;
							if (true) {
								matching34 = true;
								if (rand35 >= 0 && rand35 < 1) {
									chosen33 = true;
									iristk.situated.SystemAgentFlow.say state36 = agent.new say();
									StringCreator string37 = new StringCreator();
									string37.append("The list is as follows:");
									state36.setText(string37.toString());
									if (!flowThread.callState(state36, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 88, 13)))) {
										eventResult = EVENT_ABORTED;
										break EXECUTION;
									}
								}
							}
							if (true) {
								matching34 = true;
								if (rand35 >= 1 && rand35 < 2) {
									chosen33 = true;
									iristk.situated.SystemAgentFlow.say state38 = agent.new say();
									StringCreator string39 = new StringCreator();
									string39.append("Here's the list:");
									state38.setText(string39.toString());
									if (!flowThread.callState(state38, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 88, 13)))) {
										eventResult = EVENT_ABORTED;
										break EXECUTION;
									}
								}
							}
						}
						iristk.situated.SystemAgentFlow.say state40 = agent.new say();
						StringCreator string41 = new StringCreator();
						string41.append("1; Free fall motion. 				2; Gravity interaction between two objects of different mass. 				3; Force exerted on objects of different velocity or mass.");
						state40.setText(string41.toString());
						if (!flowThread.callState(state40, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 87, 26)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					// Line: 96
					boolean chosen42 = false;
					boolean matching43 = true;
					while (!chosen42 && matching43) {
						int rand44 = random(1365202186, 2, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
						matching43 = false;
						if (true) {
							matching43 = true;
							if (rand44 >= 0 && rand44 < 1) {
								chosen42 = true;
								iristk.situated.SystemAgentFlow.say state45 = agent.new say();
								StringCreator string46 = new StringCreator();
								string46.append("Which topic would you like to choose.");
								state45.setText(string46.toString());
								if (!flowThread.callState(state45, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 96, 12)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
							}
						}
						if (true) {
							matching43 = true;
							if (rand44 >= 1 && rand44 < 2) {
								chosen42 = true;
								iristk.situated.SystemAgentFlow.say state47 = agent.new say();
								StringCreator string48 = new StringCreator();
								string48.append("Which topic do you want to pick.");
								state47.setText(string48.toString());
								if (!flowThread.callState(state47, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 96, 12)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
							}
						}
					}
					iristk.situated.SystemAgentFlow.listen state49 = agent.new listen();
					if (!flowThread.callState(state49, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 86, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 86, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 102
			try {
				count = getCount(1651191114) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:topic")) {
						incrCount(1651191114);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 103
							topicChoice = event.get("sem:topic");
							// Line: 104
							if (eq(topicChoice,1)) {
								iristk.situated.SystemAgentFlow.say state50 = agent.new say();
								StringCreator string51 = new StringCreator();
								string51.append("Okay! Free fall it is.");
								state50.setText(string51.toString());
								if (!flowThread.callState(state50, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 104, 33)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
								// Line: 106
								FreeFallIntro state52 = new FreeFallIntro();
								flowThread.gotoState(state52, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 106, 35)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 108
							if (eq(topicChoice,2)) {
								iristk.situated.SystemAgentFlow.say state53 = agent.new say();
								StringCreator string54 = new StringCreator();
								string54.append("All right! Two objects of different mass then!");
								state53.setText(string54.toString());
								if (!flowThread.callState(state53, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 108, 33)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
								// Line: 110
								GravityIntro state55 = new GravityIntro();
								flowThread.gotoState(state55, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 110, 34)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 112
							if (eq(topicChoice,3)) {
								iristk.situated.SystemAgentFlow.say state56 = agent.new say();
								StringCreator string57 = new StringCreator();
								string57.append("Great! Force between two objects then.");
								state56.setText(string57.toString());
								if (!flowThread.callState(state56, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 112, 33)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
								// Line: 114
								ForceIntro state58 = new ForceIntro();
								flowThread.gotoState(state58, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 114, 32)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 102, 60));
			}
			// Line: 117
			try {
				count = getCount(2111991224) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:repeat")) {
						incrCount(2111991224);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 118
							repeat = event.get("sem:repeat");
							// Line: 119
							if (eq(repeat,1)) {
								iristk.situated.SystemAgentFlow.say state59 = agent.new say();
								StringCreator string60 = new StringCreator();
								string60.append("Of course!");
								state59.setText(string60.toString());
								if (!flowThread.callState(state59, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 119, 28)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
								// Line: 121
								TopicList state61 = new TopicList();
								flowThread.gotoState(state61, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 121, 31)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							iristk.situated.SystemAgentFlow.say state62 = agent.new say();
							StringCreator string63 = new StringCreator();
							string63.append("Would you like me to repeat the list?");
							state62.setText(string63.toString());
							if (!flowThread.callState(state62, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 117, 61)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 124
							Repeat state64 = new Repeat();
							state64.setSname("TopicList");
							if (!flowThread.callState(state64, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 124, 48)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 125
							flowThread.reentryState(this, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 125, 15)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 117, 61));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class FreeFallIntro extends Dialog {

		final State currentState = this;

		public Object repeat;

		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 131
			try {
				EXECUTION: {
					int count = getCount(604107971) + 1;
					incrCount(604107971);
					// Line: 132
					if (count == 1) {
						iristk.situated.SystemAgentFlow.say state65 = agent.new say();
						StringCreator string66 = new StringCreator();
						string66.append("Free fall is the state an object is in when the only force 				acting on it is gravity. For example, a ball dropped from a height will be 				in free fall until it hits the ground, ignoring the effect of air resistance.");
						state65.setText(string66.toString());
						if (!flowThread.callState(state65, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 132, 26)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					// Line: 137
					boolean chosen67 = false;
					boolean matching68 = true;
					while (!chosen67 && matching68) {
						int rand69 = random(1227229563, 2, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
						matching68 = false;
						if (true) {
							matching68 = true;
							if (rand69 >= 0 && rand69 < 1) {
								chosen67 = true;
								iristk.situated.SystemAgentFlow.say state70 = agent.new say();
								StringCreator string71 = new StringCreator();
								string71.append("Would you like to answer the question on this topic?");
								state70.setText(string71.toString());
								if (!flowThread.callState(state70, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 137, 12)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
							}
						}
						if (true) {
							matching68 = true;
							if (rand69 >= 1 && rand69 < 2) {
								chosen67 = true;
								iristk.situated.SystemAgentFlow.say state72 = agent.new say();
								StringCreator string73 = new StringCreator();
								string73.append("Do you want to answer the question on this topic?");
								state72.setText(string73.toString());
								if (!flowThread.callState(state72, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 137, 12)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
							}
						}
					}
					iristk.situated.SystemAgentFlow.listen state74 = agent.new listen();
					if (!flowThread.callState(state74, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 131, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 131, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 143
			try {
				count = getCount(1982791261) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:yes")) {
						incrCount(1982791261);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							iristk.situated.SystemAgentFlow.say state75 = agent.new say();
							StringCreator string76 = new StringCreator();
							string76.append("All right, here we go!");
							state75.setText(string76.toString());
							if (!flowThread.callState(state75, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 143, 58)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 145
							FreeFallQ1 state77 = new FreeFallQ1();
							flowThread.gotoState(state77, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 145, 31)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 143, 58));
			}
			// Line: 147
			try {
				count = getCount(1101288798) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:no")) {
						incrCount(1101288798);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							iristk.situated.SystemAgentFlow.say state78 = agent.new say();
							StringCreator string79 = new StringCreator();
							string79.append("Okay, we'll go back to the topic list.");
							state78.setText(string79.toString());
							if (!flowThread.callState(state78, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 147, 57)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 149
							TopicList state80 = new TopicList();
							flowThread.gotoState(state80, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 149, 30)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 147, 57));
			}
			// Line: 151
			try {
				count = getCount(971848845) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:repeat")) {
						incrCount(971848845);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 152
							repeat = event.get("sem:repeat");
							// Line: 153
							if (eq(repeat,1)) {
								iristk.situated.SystemAgentFlow.say state81 = agent.new say();
								StringCreator string82 = new StringCreator();
								string82.append("Of course!");
								state81.setText(string82.toString());
								if (!flowThread.callState(state81, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 153, 28)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
								// Line: 155
								FreeFallIntro state83 = new FreeFallIntro();
								flowThread.gotoState(state83, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 155, 35)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							iristk.situated.SystemAgentFlow.say state84 = agent.new say();
							StringCreator string85 = new StringCreator();
							string85.append("Would you like me to repeat the description?");
							state84.setText(string85.toString());
							if (!flowThread.callState(state84, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 151, 61)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 158
							Repeat state86 = new Repeat();
							state86.setSname("FreeFallIntro");
							if (!flowThread.callState(state86, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 158, 52)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 159
							flowThread.reentryState(this, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 159, 15)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 151, 61));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class FreeFallQ1 extends Dialog {

		final State currentState = this;

		public Object freeFallResponse;
		public Object repeat;

		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 166
			try {
				EXECUTION: {
					int count = getCount(1617791695) + 1;
					incrCount(1617791695);
					// Line: 167
					if (count == 1) {
						// Line: 168
						boolean chosen87 = false;
						boolean matching88 = true;
						while (!chosen87 && matching88) {
							int rand89 = random(1192108080, 2, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
							matching88 = false;
							if (true) {
								matching88 = true;
								if (rand89 >= 0 && rand89 < 1) {
									chosen87 = true;
									iristk.situated.SystemAgentFlow.say state90 = agent.new say();
									StringCreator string91 = new StringCreator();
									string91.append("The question then!");
									state90.setText(string91.toString());
									if (!flowThread.callState(state90, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 168, 13)))) {
										eventResult = EVENT_ABORTED;
										break EXECUTION;
									}
								}
							}
							if (true) {
								matching88 = true;
								if (rand89 >= 1 && rand89 < 2) {
									chosen87 = true;
									iristk.situated.SystemAgentFlow.say state92 = agent.new say();
									StringCreator string93 = new StringCreator();
									string93.append("Here's the question:");
									state92.setText(string93.toString());
									if (!flowThread.callState(state92, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 168, 13)))) {
										eventResult = EVENT_ABORTED;
										break EXECUTION;
									}
								}
							}
						}
					}
					iristk.situated.SystemAgentFlow.say state94 = agent.new say();
					StringCreator string95 = new StringCreator();
					// Line: 168
					if (count==1) {
						string95.append("If two balls, one twice as heavy as the other, are dropped from the 			same height at the same time, ignoring any effect of air resistance,");
					}
					string95.append("which will hit the ground first?");
					state94.setText(string95.toString());
					if (!flowThread.callState(state94, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 166, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					iristk.situated.SystemAgentFlow.listen state96 = agent.new listen();
					if (!flowThread.callState(state96, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 166, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 166, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 178
			try {
				count = getCount(1068824137) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:freefall")) {
						incrCount(1068824137);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 179
							freeFallResponse = event.get("sem:freefall");
							// Line: 180
							if (eq(freeFallResponse,1)) {
								// Line: 181
								FreeFallE1 state97 = new FreeFallE1();
								state97.setResponse(1);
								flowThread.gotoState(state97, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 181, 46)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 183
							if (eq(freeFallResponse,2)) {
								// Line: 184
								FreeFallE1 state98 = new FreeFallE1();
								state98.setResponse(2);
								flowThread.gotoState(state98, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 184, 46)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 186
							if (eq(freeFallResponse,3)) {
								// Line: 187
								FreeFallE1 state99 = new FreeFallE1();
								state99.setResponse(3);
								flowThread.gotoState(state99, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 187, 46)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 189
							if (eq(freeFallResponse,4)) {
								iristk.situated.SystemAgentFlow.say state100 = agent.new say();
								StringCreator string101 = new StringCreator();
								string101.append("That's okay");
								state100.setText(string101.toString());
								if (!flowThread.callState(state100, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 189, 38)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
								// Line: 191
								FreeFallA1 state102 = new FreeFallA1();
								state102.setResponseA(4);
								flowThread.gotoState(state102, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 191, 47)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 178, 63));
			}
			// Line: 194
			try {
				count = getCount(1705929636) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:repeat")) {
						incrCount(1705929636);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 195
							repeat = event.get("sem:repeat");
							// Line: 196
							if (eq(repeat,1)) {
								iristk.situated.SystemAgentFlow.say state103 = agent.new say();
								StringCreator string104 = new StringCreator();
								string104.append("Of course!");
								state103.setText(string104.toString());
								if (!flowThread.callState(state103, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 196, 28)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
								// Line: 198
								FreeFallQ1 state105 = new FreeFallQ1();
								flowThread.gotoState(state105, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 198, 32)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							iristk.situated.SystemAgentFlow.say state106 = agent.new say();
							StringCreator string107 = new StringCreator();
							string107.append("Would you like me to repeat the full question?");
							state106.setText(string107.toString());
							if (!flowThread.callState(state106, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 194, 61)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 201
							Repeat state108 = new Repeat();
							state108.setSname("FreeFallQ1");
							if (!flowThread.callState(state108, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 201, 49)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 202
							flowThread.reentryState(this, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 202, 15)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 194, 61));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class FreeFallE1 extends Dialog {

		final State currentState = this;
		public Integer response = null;

		public void setResponse(Object value) {
			if (value != null) {
				response = asInteger(value);
				params.put("response", value);
			}
		}


		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 208
			try {
				EXECUTION: {
					int count = getCount(1361960727) + 1;
					incrCount(1361960727);
					// Line: 209
					boolean chosen109 = false;
					boolean matching110 = true;
					while (!chosen109 && matching110) {
						int rand111 = random(739498517, 3, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
						matching110 = false;
						if (true) {
							matching110 = true;
							if (rand111 >= 0 && rand111 < 1) {
								chosen109 = true;
								iristk.situated.SystemAgentFlow.say state112 = agent.new say();
								StringCreator string113 = new StringCreator();
								string113.append("Why do you say that?");
								state112.setText(string113.toString());
								if (!flowThread.callState(state112, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 209, 12)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
							}
						}
						if (true) {
							matching110 = true;
							if (rand111 >= 1 && rand111 < 2) {
								chosen109 = true;
								iristk.situated.SystemAgentFlow.say state114 = agent.new say();
								StringCreator string115 = new StringCreator();
								string115.append("Why this choice?");
								state114.setText(string115.toString());
								if (!flowThread.callState(state114, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 209, 12)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
							}
						}
						if (true) {
							matching110 = true;
							if (rand111 >= 2 && rand111 < 3) {
								chosen109 = true;
								iristk.situated.SystemAgentFlow.say state116 = agent.new say();
								StringCreator string117 = new StringCreator();
								string117.append("Why this particular option?");
								state116.setText(string117.toString());
								if (!flowThread.callState(state116, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 209, 12)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
							}
						}
					}
					iristk.situated.SystemAgentFlow.listen state118 = agent.new listen();
					if (!flowThread.callState(state118, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 208, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 208, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 216
			try {
				count = getCount(125130493) + 1;
				if (event.triggers("sense.user.speak")) {
					incrCount(125130493);
					eventResult = EVENT_CONSUMED;
					EXECUTION: {
						// Line: 217
						boolean chosen119 = false;
						boolean matching120 = true;
						while (!chosen119 && matching120) {
							int rand121 = random(914504136, 3, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
							matching120 = false;
							if (true) {
								matching120 = true;
								if (rand121 >= 0 && rand121 < 1) {
									chosen119 = true;
									iristk.situated.SystemAgentFlow.say state122 = agent.new say();
									StringCreator string123 = new StringCreator();
									string123.append("I see");
									state122.setText(string123.toString());
									if (!flowThread.callState(state122, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 217, 12)))) {
										eventResult = EVENT_ABORTED;
										break EXECUTION;
									}
								}
							}
							if (true) {
								matching120 = true;
								if (rand121 >= 1 && rand121 < 2) {
									chosen119 = true;
									iristk.situated.SystemAgentFlow.say state124 = agent.new say();
									StringCreator string125 = new StringCreator();
									string125.append("Interesting");
									state124.setText(string125.toString());
									if (!flowThread.callState(state124, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 217, 12)))) {
										eventResult = EVENT_ABORTED;
										break EXECUTION;
									}
								}
							}
							if (true) {
								matching120 = true;
								if (rand121 >= 2 && rand121 < 3) {
									chosen119 = true;
									iristk.situated.SystemAgentFlow.say state126 = agent.new say();
									StringCreator string127 = new StringCreator();
									string127.append("Ah");
									state126.setText(string127.toString());
									if (!flowThread.callState(state126, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 217, 12)))) {
										eventResult = EVENT_ABORTED;
										break EXECUTION;
									}
								}
							}
						}
						// Line: 222
						FreeFallA1 state128 = new FreeFallA1();
						state128.setResponseA(response);
						flowThread.gotoState(state128, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 222, 54)));
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					if (eventResult != EVENT_IGNORED) return eventResult;
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 216, 36));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class FreeFallA1 extends Dialog {

		final State currentState = this;
		public Integer responseA = null;

		public void setResponseA(Object value) {
			if (value != null) {
				responseA = asInteger(value);
				params.put("responseA", value);
			}
		}

		public Object repeat;

		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 229
			try {
				EXECUTION: {
					int count = getCount(2085857771) + 1;
					incrCount(2085857771);
					// Line: 230
					if (responseA==1 && count==1) {
						iristk.situated.SystemAgentFlow.say state129 = agent.new say();
						StringCreator string130 = new StringCreator();
						string130.append("It is a common misconception that heavier objects fall faster. 				Many people think this because it just seems intuitive. Heavier objects 				take more effort to lift, so it seems this should result in them coming 				back down faster.");
						state129.setText(string130.toString());
						if (!flowThread.callState(state129, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 230, 41)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					// Line: 236
					if (responseA==2 && count==1) {
						iristk.situated.SystemAgentFlow.say state131 = agent.new say();
						StringCreator string132 = new StringCreator();
						string132.append("The idea that a lighter object would fall faster is... interesting. 				I must admit I'm not entirely sure what to make of it.");
						state131.setText(string132.toString());
						if (!flowThread.callState(state131, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 236, 41)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					// Line: 240
					if (responseA==3 && count==1) {
						iristk.situated.SystemAgentFlow.say state133 = agent.new say();
						StringCreator string134 = new StringCreator();
						string134.append("You're absolutely right.");
						state133.setText(string134.toString());
						if (!flowThread.callState(state133, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 240, 41)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					// Line: 243
					if (responseA==4 && count==1) {
						iristk.situated.SystemAgentFlow.say state135 = agent.new say();
						StringCreator string136 = new StringCreator();
						string136.append("Hopefully this will help you understand.");
						state135.setText(string136.toString());
						if (!flowThread.callState(state135, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 243, 41)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					// Line: 246
					if (count==1) {
						iristk.situated.SystemAgentFlow.say state137 = agent.new say();
						StringCreator string138 = new StringCreator();
						string138.append("The");
						// Line: 246
						if (responseA==1 || responseA==2) {
							string138.append("correct");
						}
						string138.append("answer  				is that without air resistance, all objects fall at the same rate, so they'll hit 				the ground together. This is because the acceleration due to gravity at the earth's  				surface is constant for every object regardless of its mass. However, objects with more 				mass do have a higher force applied to them. We can see this with newton's second law,  				that force equals mass times acceleration, if we rearrange it to show acceleration is equal 				to force divided by mass. The higher the mass, the higher the force must be to keep the  				acceleration constant. So really, people who think that a higher mass means more effort are  				perfectly correct! It's just that it balances out to end up with the same acceleration. 				I apologise that, as a talking head, I can't write down the law for you.");
						state137.setText(string138.toString());
						if (!flowThread.callState(state137, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 246, 24)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						iristk.situated.SystemAgentFlow.say state139 = agent.new say();
						StringCreator string140 = new StringCreator();
						string140.append("It's an interesting topic, and the second law is also important in showing  				why acceleration due to gravity is constant in the first place!");
						state139.setText(string140.toString());
						if (!flowThread.callState(state139, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 246, 24)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						iristk.situated.SystemAgentFlow.say state141 = agent.new say();
						StringCreator string142 = new StringCreator();
						string142.append("Would you like to hear about that?");
						state141.setText(string142.toString());
						if (!flowThread.callState(state141, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 246, 24)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					// Line: 261
					if (count > 1) {
						iristk.situated.SystemAgentFlow.say state143 = agent.new say();
						StringCreator string144 = new StringCreator();
						string144.append("Would you like to hear about how the second law is important in showing  				why acceleration due to gravity is constant on earth?");
						state143.setText(string144.toString());
						if (!flowThread.callState(state143, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 261, 28)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					iristk.situated.SystemAgentFlow.listen state145 = agent.new listen();
					if (!flowThread.callState(state145, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 229, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 229, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 267
			try {
				count = getCount(2001112025) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:yes")) {
						incrCount(2001112025);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 268
							boolean chosen146 = false;
							boolean matching147 = true;
							while (!chosen146 && matching147) {
								int rand148 = random(314265080, 3, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
								matching147 = false;
								if (true) {
									matching147 = true;
									if (rand148 >= 0 && rand148 < 1) {
										chosen146 = true;
										iristk.situated.SystemAgentFlow.say state149 = agent.new say();
										StringCreator string150 = new StringCreator();
										string150.append("Okay! I love explaining extra things");
										state149.setText(string150.toString());
										if (!flowThread.callState(state149, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 268, 12)))) {
											eventResult = EVENT_ABORTED;
											break EXECUTION;
										}
									}
								}
								if (true) {
									matching147 = true;
									if (rand148 >= 1 && rand148 < 2) {
										chosen146 = true;
										iristk.situated.SystemAgentFlow.say state151 = agent.new say();
										StringCreator string152 = new StringCreator();
										string152.append("All right; I like an opportunity to talk more");
										state151.setText(string152.toString());
										if (!flowThread.callState(state151, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 268, 12)))) {
											eventResult = EVENT_ABORTED;
											break EXECUTION;
										}
									}
								}
								if (true) {
									matching147 = true;
									if (rand148 >= 2 && rand148 < 3) {
										chosen146 = true;
										iristk.situated.SystemAgentFlow.say state153 = agent.new say();
										StringCreator string154 = new StringCreator();
										string154.append("Great! I like a chance to further explain concepts");
										state153.setText(string154.toString());
										if (!flowThread.callState(state153, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 268, 12)))) {
											eventResult = EVENT_ABORTED;
											break EXECUTION;
										}
									}
								}
							}
							// Line: 273
							FreeFallExtra state155 = new FreeFallExtra();
							flowThread.gotoState(state155, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 273, 34)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 267, 58));
			}
			// Line: 275
			try {
				count = getCount(2054881392) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:no")) {
						incrCount(2054881392);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							iristk.situated.SystemAgentFlow.say state156 = agent.new say();
							StringCreator string157 = new StringCreator();
							string157.append("Okay.");
							state156.setText(string157.toString());
							if (!flowThread.callState(state156, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 275, 57)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 277
							TopicOutro state158 = new TopicOutro();
							flowThread.gotoState(state158, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 277, 31)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 275, 57));
			}
			// Line: 279
			try {
				count = getCount(1908153060) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:repeat")) {
						incrCount(1908153060);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 280
							repeat = event.get("sem:repeat");
							iristk.situated.SystemAgentFlow.say state159 = agent.new say();
							StringCreator string160 = new StringCreator();
							string160.append("Would you like me to repeat the full answer?");
							state159.setText(string160.toString());
							if (!flowThread.callState(state159, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 279, 61)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 282
							Repeat state161 = new Repeat();
							state161.setSname("FreeFallA1");
							if (!flowThread.callState(state161, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 282, 49)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 283
							flowThread.reentryState(this, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 283, 15)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 279, 61));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class FreeFallExtra extends Dialog {

		final State currentState = this;

		public Object repeat;

		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 289
			try {
				EXECUTION: {
					int count = getCount(2065530879) + 1;
					incrCount(2065530879);
					// Line: 290
					if (count==1) {
						iristk.situated.SystemAgentFlow.say state162 = agent.new say();
						StringCreator string163 = new StringCreator();
						string163.append("The way to see why acceleration due to gravity is constant is to combine 				the universal law of gravitation with newton's second law. See, the universal law of 				gravitation states that the force of gravity between any two objects is the gravitational 				constant G - a very small number - multiplied by the mass of the first and then the second 				object, with the total divided by the square of the distance between them.  				We know newton's second law involves force also, so if we swap out force in the gravity 				law with mass times acceleration, we can strike one mass from either side, leaving us with 				just acceleration being equal to G times a mass, divided by a distance squared. If we take the 				mass to be the mass of the earth, and the distance to be its radius, then we have acceleration 				due to gravity on the earth's surface, and we can see it must be a constant. I hope that 				explanation made sense, and there's a little more info on this in the gravity topic.");
						state162.setText(string163.toString());
						if (!flowThread.callState(state162, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 290, 24)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					iristk.situated.SystemAgentFlow.say state164 = agent.new say();
					StringCreator string165 = new StringCreator();
					string165.append("Now, would you like to go back to the list of topics?");
					state164.setText(string165.toString());
					if (!flowThread.callState(state164, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 289, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					iristk.situated.SystemAgentFlow.listen state166 = agent.new listen();
					if (!flowThread.callState(state166, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 289, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 289, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 306
			try {
				count = getCount(943010986) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:yes")) {
						incrCount(943010986);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 307
							boolean chosen167 = false;
							boolean matching168 = true;
							while (!chosen167 && matching168) {
								int rand169 = random(1807837413, 3, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
								matching168 = false;
								if (true) {
									matching168 = true;
									if (rand169 >= 0 && rand169 < 1) {
										chosen167 = true;
										iristk.situated.SystemAgentFlow.say state170 = agent.new say();
										StringCreator string171 = new StringCreator();
										string171.append("Okay!");
										state170.setText(string171.toString());
										if (!flowThread.callState(state170, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 307, 12)))) {
											eventResult = EVENT_ABORTED;
											break EXECUTION;
										}
									}
								}
								if (true) {
									matching168 = true;
									if (rand169 >= 1 && rand169 < 2) {
										chosen167 = true;
										iristk.situated.SystemAgentFlow.say state172 = agent.new say();
										StringCreator string173 = new StringCreator();
										string173.append("All right!");
										state172.setText(string173.toString());
										if (!flowThread.callState(state172, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 307, 12)))) {
											eventResult = EVENT_ABORTED;
											break EXECUTION;
										}
									}
								}
								if (true) {
									matching168 = true;
									if (rand169 >= 2 && rand169 < 3) {
										chosen167 = true;
										iristk.situated.SystemAgentFlow.say state174 = agent.new say();
										StringCreator string175 = new StringCreator();
										string175.append("Great!");
										state174.setText(string175.toString());
										if (!flowThread.callState(state174, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 307, 12)))) {
											eventResult = EVENT_ABORTED;
											break EXECUTION;
										}
									}
								}
							}
							// Line: 312
							TopicList state176 = new TopicList();
							flowThread.gotoState(state176, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 312, 30)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 306, 58));
			}
			// Line: 314
			try {
				count = getCount(48612937) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:no")) {
						incrCount(48612937);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 315
							ExitDialog state177 = new ExitDialog();
							if (!flowThread.callState(state177, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 315, 31)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 316
							flowThread.reentryState(this, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 316, 15)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 314, 57));
			}
			// Line: 318
			try {
				count = getCount(274064559) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:repeat")) {
						incrCount(274064559);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 319
							repeat = event.get("sem:repeat");
							iristk.situated.SystemAgentFlow.say state178 = agent.new say();
							StringCreator string179 = new StringCreator();
							string179.append("Would you like me to repeat the full explanation?");
							state178.setText(string179.toString());
							if (!flowThread.callState(state178, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 318, 61)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 321
							Repeat state180 = new Repeat();
							state180.setSname("FreeFallExtra");
							if (!flowThread.callState(state180, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 321, 52)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 322
							flowThread.reentryState(this, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 322, 15)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 318, 61));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class GravityIntro extends Dialog {

		final State currentState = this;

		public Object repeat;

		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 328
			try {
				EXECUTION: {
					int count = getCount(1744347043) + 1;
					incrCount(1744347043);
					// Line: 329
					if (count == 1) {
						iristk.situated.SystemAgentFlow.say state181 = agent.new say();
						StringCreator string182 = new StringCreator();
						string182.append("Any two objects, of any mass, exert a gravitational force on each  				other. Most of the time, this force isn't noticeable, because most objects are  				quite small, but in the case of stellar objects such as planets, the effect is quite  				obvious. For example, the moon is attracted to the earth, just as the earth is  				attracted to the moon.");
						state181.setText(string182.toString());
						if (!flowThread.callState(state181, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 329, 26)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					// Line: 337
					boolean chosen183 = false;
					boolean matching184 = true;
					while (!chosen183 && matching184) {
						int rand185 = random(662441761, 2, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
						matching184 = false;
						if (true) {
							matching184 = true;
							if (rand185 >= 0 && rand185 < 1) {
								chosen183 = true;
								iristk.situated.SystemAgentFlow.say state186 = agent.new say();
								StringCreator string187 = new StringCreator();
								string187.append("Would you like to answer the question on this topic?");
								state186.setText(string187.toString());
								if (!flowThread.callState(state186, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 337, 12)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
							}
						}
						if (true) {
							matching184 = true;
							if (rand185 >= 1 && rand185 < 2) {
								chosen183 = true;
								iristk.situated.SystemAgentFlow.say state188 = agent.new say();
								StringCreator string189 = new StringCreator();
								string189.append("Do you want to answer the question on this topic?");
								state188.setText(string189.toString());
								if (!flowThread.callState(state188, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 337, 12)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
							}
						}
					}
					iristk.situated.SystemAgentFlow.listen state190 = agent.new listen();
					if (!flowThread.callState(state190, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 328, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 328, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 343
			try {
				count = getCount(1618212626) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:yes")) {
						incrCount(1618212626);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							iristk.situated.SystemAgentFlow.say state191 = agent.new say();
							StringCreator string192 = new StringCreator();
							string192.append("All right, here we go!");
							state191.setText(string192.toString());
							if (!flowThread.callState(state191, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 343, 58)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 345
							GravityQ1 state193 = new GravityQ1();
							flowThread.gotoState(state193, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 345, 30)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 343, 58));
			}
			// Line: 347
			try {
				count = getCount(1023714065) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:no")) {
						incrCount(1023714065);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							iristk.situated.SystemAgentFlow.say state194 = agent.new say();
							StringCreator string195 = new StringCreator();
							string195.append("Okay, we'll go back to the topic list.");
							state194.setText(string195.toString());
							if (!flowThread.callState(state194, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 347, 57)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 349
							TopicList state196 = new TopicList();
							flowThread.gotoState(state196, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 349, 30)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 347, 57));
			}
			// Line: 351
			try {
				count = getCount(99747242) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:repeat")) {
						incrCount(99747242);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 352
							repeat = event.get("sem:repeat");
							// Line: 353
							if (eq(repeat,1)) {
								iristk.situated.SystemAgentFlow.say state197 = agent.new say();
								StringCreator string198 = new StringCreator();
								string198.append("Of course!");
								state197.setText(string198.toString());
								if (!flowThread.callState(state197, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 353, 28)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
								// Line: 355
								GravityIntro state199 = new GravityIntro();
								flowThread.gotoState(state199, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 355, 34)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							iristk.situated.SystemAgentFlow.say state200 = agent.new say();
							StringCreator string201 = new StringCreator();
							string201.append("Would you like me to repeat the description?");
							state200.setText(string201.toString());
							if (!flowThread.callState(state200, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 351, 61)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 358
							Repeat state202 = new Repeat();
							state202.setSname("GravityIntro");
							if (!flowThread.callState(state202, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 358, 51)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 359
							flowThread.reentryState(this, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 359, 15)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 351, 61));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class GravityQ1 extends Dialog {

		final State currentState = this;

		public Object gravityResponse;
		public Object repeat;

		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 366
			try {
				EXECUTION: {
					int count = getCount(1603195447) + 1;
					incrCount(1603195447);
					// Line: 367
					if (count == 1) {
						// Line: 368
						boolean chosen203 = false;
						boolean matching204 = true;
						while (!chosen203 && matching204) {
							int rand205 = random(1191747167, 2, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
							matching204 = false;
							if (true) {
								matching204 = true;
								if (rand205 >= 0 && rand205 < 1) {
									chosen203 = true;
									iristk.situated.SystemAgentFlow.say state206 = agent.new say();
									StringCreator string207 = new StringCreator();
									string207.append("The question then!");
									state206.setText(string207.toString());
									if (!flowThread.callState(state206, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 368, 13)))) {
										eventResult = EVENT_ABORTED;
										break EXECUTION;
									}
								}
							}
							if (true) {
								matching204 = true;
								if (rand205 >= 1 && rand205 < 2) {
									chosen203 = true;
									iristk.situated.SystemAgentFlow.say state208 = agent.new say();
									StringCreator string209 = new StringCreator();
									string209.append("Here's the question:");
									state208.setText(string209.toString());
									if (!flowThread.callState(state208, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 368, 13)))) {
										eventResult = EVENT_ABORTED;
										break EXECUTION;
									}
								}
							}
						}
					}
					iristk.situated.SystemAgentFlow.say state210 = agent.new say();
					StringCreator string211 = new StringCreator();
					// Line: 368
					if (count==1) {
						string211.append("In the example from the intro, of the attraction  			between the earth and the moon,");
					}
					string211.append("which exerts the greater attractive force,  			on the other, the earth, or the moon?");
					state210.setText(string211.toString());
					if (!flowThread.callState(state210, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 366, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					iristk.situated.SystemAgentFlow.listen state212 = agent.new listen();
					if (!flowThread.callState(state212, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 366, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 366, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 378
			try {
				count = getCount(1094834071) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:gravity")) {
						incrCount(1094834071);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 379
							gravityResponse = event.get("sem:gravity");
							// Line: 380
							if (eq(gravityResponse,1)) {
								// Line: 381
								GravityE1 state213 = new GravityE1();
								state213.setResponse(1);
								flowThread.gotoState(state213, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 381, 45)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 383
							if (eq(gravityResponse,2)) {
								// Line: 384
								GravityE1 state214 = new GravityE1();
								state214.setResponse(2);
								flowThread.gotoState(state214, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 384, 45)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 386
							if (eq(gravityResponse,3)) {
								// Line: 387
								GravityE1 state215 = new GravityE1();
								state215.setResponse(3);
								flowThread.gotoState(state215, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 387, 45)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 389
							if (eq(gravityResponse,4)) {
								iristk.situated.SystemAgentFlow.say state216 = agent.new say();
								StringCreator string217 = new StringCreator();
								string217.append("That's okay");
								state216.setText(string217.toString());
								if (!flowThread.callState(state216, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 389, 37)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
								// Line: 391
								GravityA1 state218 = new GravityA1();
								state218.setResponseA(4);
								flowThread.gotoState(state218, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 391, 46)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 378, 62));
			}
			// Line: 394
			try {
				count = getCount(766572210) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:repeat")) {
						incrCount(766572210);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 395
							repeat = event.get("sem:repeat");
							// Line: 396
							if (eq(repeat,1)) {
								iristk.situated.SystemAgentFlow.say state219 = agent.new say();
								StringCreator string220 = new StringCreator();
								string220.append("Of course!");
								state219.setText(string220.toString());
								if (!flowThread.callState(state219, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 396, 28)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
								// Line: 398
								GravityQ1 state221 = new GravityQ1();
								flowThread.gotoState(state221, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 398, 31)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							iristk.situated.SystemAgentFlow.say state222 = agent.new say();
							StringCreator string223 = new StringCreator();
							string223.append("Would you like me to repeat the full question?");
							state222.setText(string223.toString());
							if (!flowThread.callState(state222, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 394, 61)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 401
							Repeat state224 = new Repeat();
							state224.setSname("GravityQ1");
							if (!flowThread.callState(state224, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 401, 48)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 402
							flowThread.reentryState(this, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 402, 15)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 394, 61));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class GravityE1 extends Dialog {

		final State currentState = this;
		public Integer response = null;

		public void setResponse(Object value) {
			if (value != null) {
				response = asInteger(value);
				params.put("response", value);
			}
		}

		public Object repeat;

		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 409
			try {
				EXECUTION: {
					int count = getCount(380936215) + 1;
					incrCount(380936215);
					// Line: 410
					boolean chosen225 = false;
					boolean matching226 = true;
					while (!chosen225 && matching226) {
						int rand227 = random(142638629, 3, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
						matching226 = false;
						if (true) {
							matching226 = true;
							if (rand227 >= 0 && rand227 < 1) {
								chosen225 = true;
								iristk.situated.SystemAgentFlow.say state228 = agent.new say();
								StringCreator string229 = new StringCreator();
								string229.append("Why do you say that?");
								state228.setText(string229.toString());
								if (!flowThread.callState(state228, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 410, 12)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
							}
						}
						if (true) {
							matching226 = true;
							if (rand227 >= 1 && rand227 < 2) {
								chosen225 = true;
								iristk.situated.SystemAgentFlow.say state230 = agent.new say();
								StringCreator string231 = new StringCreator();
								string231.append("Why this choice?");
								state230.setText(string231.toString());
								if (!flowThread.callState(state230, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 410, 12)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
							}
						}
						if (true) {
							matching226 = true;
							if (rand227 >= 2 && rand227 < 3) {
								chosen225 = true;
								iristk.situated.SystemAgentFlow.say state232 = agent.new say();
								StringCreator string233 = new StringCreator();
								string233.append("Why this particular option?");
								state232.setText(string233.toString());
								if (!flowThread.callState(state232, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 410, 12)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
							}
						}
					}
					iristk.situated.SystemAgentFlow.listen state234 = agent.new listen();
					if (!flowThread.callState(state234, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 409, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 409, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 417
			try {
				count = getCount(707806938) + 1;
				if (event.triggers("sense.user.speak")) {
					incrCount(707806938);
					eventResult = EVENT_CONSUMED;
					EXECUTION: {
						// Line: 418
						boolean chosen235 = false;
						boolean matching236 = true;
						while (!chosen235 && matching236) {
							int rand237 = random(705265961, 3, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
							matching236 = false;
							if (true) {
								matching236 = true;
								if (rand237 >= 0 && rand237 < 1) {
									chosen235 = true;
									iristk.situated.SystemAgentFlow.say state238 = agent.new say();
									StringCreator string239 = new StringCreator();
									string239.append("I see");
									state238.setText(string239.toString());
									if (!flowThread.callState(state238, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 418, 12)))) {
										eventResult = EVENT_ABORTED;
										break EXECUTION;
									}
								}
							}
							if (true) {
								matching236 = true;
								if (rand237 >= 1 && rand237 < 2) {
									chosen235 = true;
									iristk.situated.SystemAgentFlow.say state240 = agent.new say();
									StringCreator string241 = new StringCreator();
									string241.append("Interesting");
									state240.setText(string241.toString());
									if (!flowThread.callState(state240, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 418, 12)))) {
										eventResult = EVENT_ABORTED;
										break EXECUTION;
									}
								}
							}
							if (true) {
								matching236 = true;
								if (rand237 >= 2 && rand237 < 3) {
									chosen235 = true;
									iristk.situated.SystemAgentFlow.say state242 = agent.new say();
									StringCreator string243 = new StringCreator();
									string243.append("Ah");
									state242.setText(string243.toString());
									if (!flowThread.callState(state242, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 418, 12)))) {
										eventResult = EVENT_ABORTED;
										break EXECUTION;
									}
								}
							}
						}
						// Line: 423
						GravityA1 state244 = new GravityA1();
						state244.setResponseA(response);
						flowThread.gotoState(state244, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 423, 53)));
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					if (eventResult != EVENT_IGNORED) return eventResult;
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 417, 36));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class GravityA1 extends Dialog {

		final State currentState = this;
		public Integer responseA = null;

		public void setResponseA(Object value) {
			if (value != null) {
				responseA = asInteger(value);
				params.put("responseA", value);
			}
		}

		public Object repeat;

		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 430
			try {
				EXECUTION: {
					int count = getCount(874088044) + 1;
					incrCount(874088044);
					// Line: 431
					if (responseA==1 && count==1) {
						iristk.situated.SystemAgentFlow.say state245 = agent.new say();
						StringCreator string246 = new StringCreator();
						string246.append("Many people do tend to say that the earth will have the larger attractive  				force, because it is larger and has more mass. There's also the instinctive idea that  				things are attracted to the earth, but not vice versa.");
						state245.setText(string246.toString());
						if (!flowThread.callState(state245, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 431, 41)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					// Line: 436
					if (responseA==2 && count==1) {
						iristk.situated.SystemAgentFlow.say state247 = agent.new say();
						StringCreator string248 = new StringCreator();
						string248.append("It's not often someone will suggest that the moon exerts the greater  				attractive force.");
						state247.setText(string248.toString());
						if (!flowThread.callState(state247, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 436, 41)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					// Line: 440
					if (responseA==3 && count==1) {
						iristk.situated.SystemAgentFlow.say state249 = agent.new say();
						StringCreator string250 = new StringCreator();
						string250.append("You're correct, the force is the same.");
						state249.setText(string250.toString());
						if (!flowThread.callState(state249, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 440, 41)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					// Line: 443
					if (responseA==4 && count==1) {
						iristk.situated.SystemAgentFlow.say state251 = agent.new say();
						StringCreator string252 = new StringCreator();
						string252.append("Hopefully this will help you understand.");
						state251.setText(string252.toString());
						if (!flowThread.callState(state251, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 443, 41)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					// Line: 446
					if (count==1) {
						iristk.situated.SystemAgentFlow.say state253 = agent.new say();
						StringCreator string254 = new StringCreator();
						string254.append("The");
						// Line: 446
						if (responseA==1 || responseA==2) {
							string254.append("right");
						}
						string254.append("solution");
						// Line: 446
						if (responseA==1 || responseA==2) {
							string254.append(", however,");
						}
						string254.append("is that  				both the moon and the earth feel the same force from each other. This is the  				universal law of gravitation, that's also explained in the extra part of the  				free fall section. The gravitational force between two objects is proportional  				to the product of the mass of the two objects, and inversely proportional to  				the square of the distance between them. The reason the earth appears to have  				a stronger effect on the moon is simply that the moon is smaller. The force  				between them may be the same, but because the moon has less mass, it accelerates  				towards the earth faster than the earth accelerates towards it, consistent with newton's  				second law that force equals mass times acceleration.");
						state253.setText(string254.toString());
						if (!flowThread.callState(state253, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 446, 24)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						iristk.situated.SystemAgentFlow.say state255 = agent.new say();
						StringCreator string256 = new StringCreator();
						string256.append("I have a brief extra explainer with an interesting point about why the moon doesn't  				hit the earth, and also about what force a person exerts on the planet.");
						state255.setText(string256.toString());
						if (!flowThread.callState(state255, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 446, 24)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					iristk.situated.SystemAgentFlow.say state257 = agent.new say();
					StringCreator string258 = new StringCreator();
					string258.append("Would you like to hear it?");
					state257.setText(string258.toString());
					if (!flowThread.callState(state257, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 430, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					iristk.situated.SystemAgentFlow.listen state259 = agent.new listen();
					if (!flowThread.callState(state259, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 430, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 430, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 465
			try {
				count = getCount(1749186397) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:yes")) {
						incrCount(1749186397);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 466
							boolean chosen260 = false;
							boolean matching261 = true;
							while (!chosen260 && matching261) {
								int rand262 = random(1464642111, 3, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
								matching261 = false;
								if (true) {
									matching261 = true;
									if (rand262 >= 0 && rand262 < 1) {
										chosen260 = true;
										iristk.situated.SystemAgentFlow.say state263 = agent.new say();
										StringCreator string264 = new StringCreator();
										string264.append("Okay! I like to talk more");
										state263.setText(string264.toString());
										if (!flowThread.callState(state263, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 466, 12)))) {
											eventResult = EVENT_ABORTED;
											break EXECUTION;
										}
									}
								}
								if (true) {
									matching261 = true;
									if (rand262 >= 1 && rand262 < 2) {
										chosen260 = true;
										iristk.situated.SystemAgentFlow.say state265 = agent.new say();
										StringCreator string266 = new StringCreator();
										string266.append("All right; I love a chance to explain stuff");
										state265.setText(string266.toString());
										if (!flowThread.callState(state265, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 466, 12)))) {
											eventResult = EVENT_ABORTED;
											break EXECUTION;
										}
									}
								}
								if (true) {
									matching261 = true;
									if (rand262 >= 2 && rand262 < 3) {
										chosen260 = true;
										iristk.situated.SystemAgentFlow.say state267 = agent.new say();
										StringCreator string268 = new StringCreator();
										string268.append("Great! Always up for more explaining");
										state267.setText(string268.toString());
										if (!flowThread.callState(state267, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 466, 12)))) {
											eventResult = EVENT_ABORTED;
											break EXECUTION;
										}
									}
								}
							}
							// Line: 471
							GravityExtra state269 = new GravityExtra();
							flowThread.gotoState(state269, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 471, 33)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 465, 58));
			}
			// Line: 473
			try {
				count = getCount(392292416) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:no")) {
						incrCount(392292416);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							iristk.situated.SystemAgentFlow.say state270 = agent.new say();
							StringCreator string271 = new StringCreator();
							string271.append("Okay.");
							state270.setText(string271.toString());
							if (!flowThread.callState(state270, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 473, 57)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 475
							TopicOutro state272 = new TopicOutro();
							flowThread.gotoState(state272, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 475, 31)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 473, 57));
			}
			// Line: 477
			try {
				count = getCount(1590550415) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:repeat")) {
						incrCount(1590550415);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 478
							repeat = event.get("sem:repeat");
							iristk.situated.SystemAgentFlow.say state273 = agent.new say();
							StringCreator string274 = new StringCreator();
							string274.append("Would you like me to repeat the full answer?");
							state273.setText(string274.toString());
							if (!flowThread.callState(state273, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 477, 61)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 480
							Repeat state275 = new Repeat();
							state275.setSname("GravityExtra");
							if (!flowThread.callState(state275, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 480, 51)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 481
							flowThread.reentryState(this, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 481, 15)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 477, 61));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class GravityExtra extends Dialog {

		final State currentState = this;

		public Object repeat;

		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 487
			try {
				EXECUTION: {
					int count = getCount(692342133) + 1;
					incrCount(692342133);
					// Line: 488
					if (count==1) {
						iristk.situated.SystemAgentFlow.say state276 = agent.new say();
						StringCreator string277 = new StringCreator();
						string277.append("With the earth and the moon, you might wonder, how come the moon doesn't  				crash into the earth? The answer is that it's also moving sideways as it falls, so it  				ends up circling instead, in an orbit. It's like if you threw a ball very hard, so fast  				that by the time it dropped the two metres or so to the ground, the curve of the earth  				meant the ground was two metres away again. That ball would travel right round the world  				and hit you in the back of the head! That's basically what the moon is doing - continually  				falling and missing the earth. As I said earlier, the earth does also accelerate a little  				towards the moon - it wobbles a little in its own orbit around the sun. The tides are also an  				effect of the moon's gravity! On a related note, you also exert the exact same force  				on the earth as the earth does on you. So when you jump on the spot, as you free fall back down  				like in the free fall topic, the earth very slightly accelerates towards you. But because the  				earth is so huge, the acceleration is so tiny no one would ever notice it.");
						state276.setText(string277.toString());
						if (!flowThread.callState(state276, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 488, 24)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					iristk.situated.SystemAgentFlow.say state278 = agent.new say();
					StringCreator string279 = new StringCreator();
					string279.append("Now, would you like to go back to the list of topics?");
					state278.setText(string279.toString());
					if (!flowThread.callState(state278, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 487, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					iristk.situated.SystemAgentFlow.listen state280 = agent.new listen();
					if (!flowThread.callState(state280, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 487, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 487, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 505
			try {
				count = getCount(353842779) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:yes")) {
						incrCount(353842779);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 506
							boolean chosen281 = false;
							boolean matching282 = true;
							while (!chosen281 && matching282) {
								int rand283 = random(1156060786, 3, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
								matching282 = false;
								if (true) {
									matching282 = true;
									if (rand283 >= 0 && rand283 < 1) {
										chosen281 = true;
										iristk.situated.SystemAgentFlow.say state284 = agent.new say();
										StringCreator string285 = new StringCreator();
										string285.append("Okay!");
										state284.setText(string285.toString());
										if (!flowThread.callState(state284, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 506, 12)))) {
											eventResult = EVENT_ABORTED;
											break EXECUTION;
										}
									}
								}
								if (true) {
									matching282 = true;
									if (rand283 >= 1 && rand283 < 2) {
										chosen281 = true;
										iristk.situated.SystemAgentFlow.say state286 = agent.new say();
										StringCreator string287 = new StringCreator();
										string287.append("All right!");
										state286.setText(string287.toString());
										if (!flowThread.callState(state286, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 506, 12)))) {
											eventResult = EVENT_ABORTED;
											break EXECUTION;
										}
									}
								}
								if (true) {
									matching282 = true;
									if (rand283 >= 2 && rand283 < 3) {
										chosen281 = true;
										iristk.situated.SystemAgentFlow.say state288 = agent.new say();
										StringCreator string289 = new StringCreator();
										string289.append("Great!");
										state288.setText(string289.toString());
										if (!flowThread.callState(state288, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 506, 12)))) {
											eventResult = EVENT_ABORTED;
											break EXECUTION;
										}
									}
								}
							}
							// Line: 511
							TopicList state290 = new TopicList();
							flowThread.gotoState(state290, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 511, 30)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 505, 58));
			}
			// Line: 513
			try {
				count = getCount(1286084959) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:no")) {
						incrCount(1286084959);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 514
							ExitDialog state291 = new ExitDialog();
							if (!flowThread.callState(state291, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 514, 31)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 515
							flowThread.reentryState(this, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 515, 15)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 513, 57));
			}
			// Line: 517
			try {
				count = getCount(184966243) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:repeat")) {
						incrCount(184966243);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 518
							repeat = event.get("sem:repeat");
							iristk.situated.SystemAgentFlow.say state292 = agent.new say();
							StringCreator string293 = new StringCreator();
							string293.append("Would you like me to repeat the full explanation?");
							state292.setText(string293.toString());
							if (!flowThread.callState(state292, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 517, 61)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 520
							Repeat state294 = new Repeat();
							state294.setSname("GravityExtra");
							if (!flowThread.callState(state294, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 520, 51)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 521
							flowThread.reentryState(this, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 521, 15)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 517, 61));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class ForceIntro extends Dialog {

		final State currentState = this;

		public Object repeat;

		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 527
			try {
				EXECUTION: {
					int count = getCount(1712536284) + 1;
					incrCount(1712536284);
					// Line: 528
					if (count == 1) {
						iristk.situated.SystemAgentFlow.say state295 = agent.new say();
						StringCreator string296 = new StringCreator();
						string296.append("In newtonian mechanics, whenever two objects interact, force 				and energy is transferred between them. For example, when a tennis ball is hit  				with a racket, force is applied to both the ball and the racket.");
						state295.setText(string296.toString());
						if (!flowThread.callState(state295, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 528, 26)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					// Line: 533
					boolean chosen297 = false;
					boolean matching298 = true;
					while (!chosen297 && matching298) {
						int rand299 = random(1123225098, 2, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
						matching298 = false;
						if (true) {
							matching298 = true;
							if (rand299 >= 0 && rand299 < 1) {
								chosen297 = true;
								iristk.situated.SystemAgentFlow.say state300 = agent.new say();
								StringCreator string301 = new StringCreator();
								string301.append("Would you like to answer the first question on this topic?");
								state300.setText(string301.toString());
								if (!flowThread.callState(state300, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 533, 12)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
							}
						}
						if (true) {
							matching298 = true;
							if (rand299 >= 1 && rand299 < 2) {
								chosen297 = true;
								iristk.situated.SystemAgentFlow.say state302 = agent.new say();
								StringCreator string303 = new StringCreator();
								string303.append("Do you want to answer the first question on this topic?");
								state302.setText(string303.toString());
								if (!flowThread.callState(state302, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 533, 12)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
							}
						}
					}
					iristk.situated.SystemAgentFlow.listen state304 = agent.new listen();
					if (!flowThread.callState(state304, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 527, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 527, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 539
			try {
				count = getCount(606548741) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:yes")) {
						incrCount(606548741);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							iristk.situated.SystemAgentFlow.say state305 = agent.new say();
							StringCreator string306 = new StringCreator();
							string306.append("All right, here we go!");
							state305.setText(string306.toString());
							if (!flowThread.callState(state305, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 539, 58)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 541
							ForceQ1 state307 = new ForceQ1();
							flowThread.gotoState(state307, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 541, 28)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 539, 58));
			}
			// Line: 543
			try {
				count = getCount(1190524793) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:no")) {
						incrCount(1190524793);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							iristk.situated.SystemAgentFlow.say state308 = agent.new say();
							StringCreator string309 = new StringCreator();
							string309.append("Okay, we'll go back to the topic list.");
							state308.setText(string309.toString());
							if (!flowThread.callState(state308, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 543, 57)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 545
							TopicList state310 = new TopicList();
							flowThread.gotoState(state310, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 545, 30)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 543, 57));
			}
			// Line: 547
			try {
				count = getCount(26117480) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:repeat")) {
						incrCount(26117480);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 548
							repeat = event.get("sem:repeat");
							// Line: 549
							if (eq(repeat,1)) {
								iristk.situated.SystemAgentFlow.say state311 = agent.new say();
								StringCreator string312 = new StringCreator();
								string312.append("Of course!");
								state311.setText(string312.toString());
								if (!flowThread.callState(state311, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 549, 28)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
								// Line: 551
								ForceIntro state313 = new ForceIntro();
								flowThread.gotoState(state313, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 551, 32)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							iristk.situated.SystemAgentFlow.say state314 = agent.new say();
							StringCreator string315 = new StringCreator();
							string315.append("Would you like me to repeat the description?");
							state314.setText(string315.toString());
							if (!flowThread.callState(state314, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 547, 61)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 554
							Repeat state316 = new Repeat();
							state316.setSname("ForceIntro");
							if (!flowThread.callState(state316, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 554, 49)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 555
							flowThread.reentryState(this, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 555, 15)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 547, 61));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class ForceQ1 extends Dialog {

		final State currentState = this;

		public Object forceResponse;
		public Object repeat;

		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 562
			try {
				EXECUTION: {
					int count = getCount(314337396) + 1;
					incrCount(314337396);
					// Line: 563
					if (count == 1) {
						// Line: 564
						boolean chosen317 = false;
						boolean matching318 = true;
						while (!chosen317 && matching318) {
							int rand319 = random(1282788025, 2, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
							matching318 = false;
							if (true) {
								matching318 = true;
								if (rand319 >= 0 && rand319 < 1) {
									chosen317 = true;
									iristk.situated.SystemAgentFlow.say state320 = agent.new say();
									StringCreator string321 = new StringCreator();
									string321.append("The question then!");
									state320.setText(string321.toString());
									if (!flowThread.callState(state320, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 564, 13)))) {
										eventResult = EVENT_ABORTED;
										break EXECUTION;
									}
								}
							}
							if (true) {
								matching318 = true;
								if (rand319 >= 1 && rand319 < 2) {
									chosen317 = true;
									iristk.situated.SystemAgentFlow.say state322 = agent.new say();
									StringCreator string323 = new StringCreator();
									string323.append("Here's the question:");
									state322.setText(string323.toString());
									if (!flowThread.callState(state322, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 564, 13)))) {
										eventResult = EVENT_ABORTED;
										break EXECUTION;
									}
								}
							}
						}
					}
					iristk.situated.SystemAgentFlow.say state324 = agent.new say();
					StringCreator string325 = new StringCreator();
					// Line: 564
					if (count==1) {
						string325.append("In the previous example, of a tennis ball being  			hit by a racket during a serve, where the ball is almost stationary and the  			racket travelling at speed,");
					}
					string325.append("which will have the greater force applied  			to it at the moment of contact, the racket or the ball?");
					state324.setText(string325.toString());
					if (!flowThread.callState(state324, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 562, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					iristk.situated.SystemAgentFlow.listen state326 = agent.new listen();
					if (!flowThread.callState(state326, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 562, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 562, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 575
			try {
				count = getCount(519569038) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:force1")) {
						incrCount(519569038);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 576
							forceResponse = event.get("sem:force1");
							// Line: 577
							if (eq(forceResponse,1)) {
								// Line: 578
								ForceE1 state327 = new ForceE1();
								state327.setResponse(1);
								flowThread.gotoState(state327, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 578, 43)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 580
							if (eq(forceResponse,2)) {
								// Line: 581
								ForceE1 state328 = new ForceE1();
								state328.setResponse(2);
								flowThread.gotoState(state328, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 581, 43)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 583
							if (eq(forceResponse,3)) {
								// Line: 584
								ForceE1 state329 = new ForceE1();
								state329.setResponse(3);
								flowThread.gotoState(state329, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 584, 43)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 586
							if (eq(forceResponse,4)) {
								iristk.situated.SystemAgentFlow.say state330 = agent.new say();
								StringCreator string331 = new StringCreator();
								string331.append("That's okay");
								state330.setText(string331.toString());
								if (!flowThread.callState(state330, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 586, 35)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
								// Line: 588
								ForceA1 state332 = new ForceA1();
								state332.setResponseA(4);
								flowThread.gotoState(state332, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 588, 44)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 575, 61));
			}
			// Line: 591
			try {
				count = getCount(225493257) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:repeat")) {
						incrCount(225493257);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 592
							repeat = event.get("sem:repeat");
							// Line: 593
							if (eq(repeat,1)) {
								iristk.situated.SystemAgentFlow.say state333 = agent.new say();
								StringCreator string334 = new StringCreator();
								string334.append("Of course!");
								state333.setText(string334.toString());
								if (!flowThread.callState(state333, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 593, 28)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
								// Line: 595
								ForceQ1 state335 = new ForceQ1();
								flowThread.gotoState(state335, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 595, 29)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							iristk.situated.SystemAgentFlow.say state336 = agent.new say();
							StringCreator string337 = new StringCreator();
							string337.append("Would you like me to repeat the full question?");
							state336.setText(string337.toString());
							if (!flowThread.callState(state336, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 591, 61)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 598
							Repeat state338 = new Repeat();
							state338.setSname("ForceQ1");
							if (!flowThread.callState(state338, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 598, 46)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 599
							flowThread.reentryState(this, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 599, 15)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 591, 61));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class ForceE1 extends Dialog {

		final State currentState = this;
		public Integer response = null;

		public void setResponse(Object value) {
			if (value != null) {
				response = asInteger(value);
				params.put("response", value);
			}
		}

		public Object repeat;

		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 606
			try {
				EXECUTION: {
					int count = getCount(1654589030) + 1;
					incrCount(1654589030);
					// Line: 607
					boolean chosen339 = false;
					boolean matching340 = true;
					while (!chosen339 && matching340) {
						int rand341 = random(466002798, 3, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
						matching340 = false;
						if (true) {
							matching340 = true;
							if (rand341 >= 0 && rand341 < 1) {
								chosen339 = true;
								iristk.situated.SystemAgentFlow.say state342 = agent.new say();
								StringCreator string343 = new StringCreator();
								string343.append("Why do you say that?");
								state342.setText(string343.toString());
								if (!flowThread.callState(state342, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 607, 12)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
							}
						}
						if (true) {
							matching340 = true;
							if (rand341 >= 1 && rand341 < 2) {
								chosen339 = true;
								iristk.situated.SystemAgentFlow.say state344 = agent.new say();
								StringCreator string345 = new StringCreator();
								string345.append("Why this choice?");
								state344.setText(string345.toString());
								if (!flowThread.callState(state344, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 607, 12)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
							}
						}
						if (true) {
							matching340 = true;
							if (rand341 >= 2 && rand341 < 3) {
								chosen339 = true;
								iristk.situated.SystemAgentFlow.say state346 = agent.new say();
								StringCreator string347 = new StringCreator();
								string347.append("Why this particular option?");
								state346.setText(string347.toString());
								if (!flowThread.callState(state346, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 607, 12)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
							}
						}
					}
					iristk.situated.SystemAgentFlow.listen state348 = agent.new listen();
					if (!flowThread.callState(state348, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 606, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 606, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 614
			try {
				count = getCount(33524623) + 1;
				if (event.triggers("sense.user.speak")) {
					incrCount(33524623);
					eventResult = EVENT_CONSUMED;
					EXECUTION: {
						// Line: 615
						boolean chosen349 = false;
						boolean matching350 = true;
						while (!chosen349 && matching350) {
							int rand351 = random(947679291, 3, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
							matching350 = false;
							if (true) {
								matching350 = true;
								if (rand351 >= 0 && rand351 < 1) {
									chosen349 = true;
									iristk.situated.SystemAgentFlow.say state352 = agent.new say();
									StringCreator string353 = new StringCreator();
									string353.append("I see");
									state352.setText(string353.toString());
									if (!flowThread.callState(state352, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 615, 12)))) {
										eventResult = EVENT_ABORTED;
										break EXECUTION;
									}
								}
							}
							if (true) {
								matching350 = true;
								if (rand351 >= 1 && rand351 < 2) {
									chosen349 = true;
									iristk.situated.SystemAgentFlow.say state354 = agent.new say();
									StringCreator string355 = new StringCreator();
									string355.append("Interesting");
									state354.setText(string355.toString());
									if (!flowThread.callState(state354, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 615, 12)))) {
										eventResult = EVENT_ABORTED;
										break EXECUTION;
									}
								}
							}
							if (true) {
								matching350 = true;
								if (rand351 >= 2 && rand351 < 3) {
									chosen349 = true;
									iristk.situated.SystemAgentFlow.say state356 = agent.new say();
									StringCreator string357 = new StringCreator();
									string357.append("Ah");
									state356.setText(string357.toString());
									if (!flowThread.callState(state356, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 615, 12)))) {
										eventResult = EVENT_ABORTED;
										break EXECUTION;
									}
								}
							}
						}
						// Line: 620
						ForceA1 state358 = new ForceA1();
						state358.setResponseA(response);
						flowThread.gotoState(state358, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 620, 51)));
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					if (eventResult != EVENT_IGNORED) return eventResult;
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 614, 36));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class ForceA1 extends Dialog {

		final State currentState = this;
		public Integer responseA = null;

		public void setResponseA(Object value) {
			if (value != null) {
				responseA = asInteger(value);
				params.put("responseA", value);
			}
		}

		public Object repeat;

		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 627
			try {
				EXECUTION: {
					int count = getCount(1595212853) + 1;
					incrCount(1595212853);
					// Line: 628
					if (responseA==1 && count==1) {
						iristk.situated.SystemAgentFlow.say state359 = agent.new say();
						StringCreator string360 = new StringCreator();
						string360.append("A lot of people tend to say the ball will have the larger force  				applied, because of course it is smaller and lighter than the racket, and  				the racket hits it at speed, whereas the ball starts off almost stationary.");
						state359.setText(string360.toString());
						if (!flowThread.callState(state359, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 628, 41)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					// Line: 633
					if (responseA==2 && count==1) {
						iristk.situated.SystemAgentFlow.say state361 = agent.new say();
						StringCreator string362 = new StringCreator();
						string362.append("The racket is an interesting choice. There is certainly something to be said for the idea  				the racket has the greater force applied. After all, it is travelling at speed and  				has considerably more mass.");
						state361.setText(string362.toString());
						if (!flowThread.callState(state361, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 633, 41)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					// Line: 638
					if (responseA==3 && count==1) {
						iristk.situated.SystemAgentFlow.say state363 = agent.new say();
						StringCreator string364 = new StringCreator();
						string364.append("You're right that the force is the same.");
						state363.setText(string364.toString());
						if (!flowThread.callState(state363, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 638, 41)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					// Line: 641
					if (responseA==4 && count==1) {
						iristk.situated.SystemAgentFlow.say state365 = agent.new say();
						StringCreator string366 = new StringCreator();
						string366.append("Hopefully this will help you understand.");
						state365.setText(string366.toString());
						if (!flowThread.callState(state365, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 641, 41)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					// Line: 644
					if (count==1) {
						iristk.situated.SystemAgentFlow.say state367 = agent.new say();
						StringCreator string368 = new StringCreator();
						string368.append("The");
						// Line: 644
						if (responseA==1 || responseA==2) {
							string368.append("correct");
						}
						string368.append("answer");
						// Line: 644
						if (responseA==1 || responseA==2) {
							string368.append(", however,");
						}
						string368.append("is that  				the force applied to the racket and the ball is exactly the same, just in  				opposing directions. This is newton's third law - every action has an equal  				and opposite reaction. All forces come in action - reaction pairs. You can see  				this more easily with rockets in space: there's no air in space so a rocket's  				exhaust clearly can't push off against it. Instead, the force of the exhaust  				going backwards is equalled by a force pushing back against the rocket, propelling  				it forwards.");
						state367.setText(string368.toString());
						if (!flowThread.callState(state367, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 644, 24)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					iristk.situated.SystemAgentFlow.say state369 = agent.new say();
					StringCreator string370 = new StringCreator();
					string370.append("I have a second question on this topic, concerning acceleration due to  			action - reaction pairs. Would you like to answer it?");
					state369.setText(string370.toString());
					if (!flowThread.callState(state369, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 627, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					iristk.situated.SystemAgentFlow.listen state371 = agent.new listen();
					if (!flowThread.callState(state371, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 627, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 627, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 659
			try {
				count = getCount(257895351) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:yes")) {
						incrCount(257895351);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 660
							boolean chosen372 = false;
							boolean matching373 = true;
							while (!chosen372 && matching373) {
								int rand374 = random(1929600551, 3, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
								matching373 = false;
								if (true) {
									matching373 = true;
									if (rand374 >= 0 && rand374 < 1) {
										chosen372 = true;
										iristk.situated.SystemAgentFlow.say state375 = agent.new say();
										StringCreator string376 = new StringCreator();
										string376.append("Okay! A second question then");
										state375.setText(string376.toString());
										if (!flowThread.callState(state375, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 660, 12)))) {
											eventResult = EVENT_ABORTED;
											break EXECUTION;
										}
									}
								}
								if (true) {
									matching373 = true;
									if (rand374 >= 1 && rand374 < 2) {
										chosen372 = true;
										iristk.situated.SystemAgentFlow.say state377 = agent.new say();
										StringCreator string378 = new StringCreator();
										string378.append("All right; I like asking questions");
										state377.setText(string378.toString());
										if (!flowThread.callState(state377, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 660, 12)))) {
											eventResult = EVENT_ABORTED;
											break EXECUTION;
										}
									}
								}
								if (true) {
									matching373 = true;
									if (rand374 >= 2 && rand374 < 3) {
										chosen372 = true;
										iristk.situated.SystemAgentFlow.say state379 = agent.new say();
										StringCreator string380 = new StringCreator();
										string380.append("Great! I like a chance to ask more questions");
										state379.setText(string380.toString());
										if (!flowThread.callState(state379, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 660, 12)))) {
											eventResult = EVENT_ABORTED;
											break EXECUTION;
										}
									}
								}
							}
							// Line: 665
							ForceQ2 state381 = new ForceQ2();
							flowThread.gotoState(state381, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 665, 28)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 659, 58));
			}
			// Line: 667
			try {
				count = getCount(1053782781) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:no")) {
						incrCount(1053782781);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							iristk.situated.SystemAgentFlow.say state382 = agent.new say();
							StringCreator string383 = new StringCreator();
							string383.append("Okay.");
							state382.setText(string383.toString());
							if (!flowThread.callState(state382, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 667, 57)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 669
							TopicOutro state384 = new TopicOutro();
							flowThread.gotoState(state384, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 669, 31)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 667, 57));
			}
			// Line: 671
			try {
				count = getCount(564160838) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:repeat")) {
						incrCount(564160838);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 672
							repeat = event.get("sem:repeat");
							iristk.situated.SystemAgentFlow.say state385 = agent.new say();
							StringCreator string386 = new StringCreator();
							string386.append("Would you like me to repeat the full answer?");
							state385.setText(string386.toString());
							if (!flowThread.callState(state385, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 671, 61)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 674
							Repeat state387 = new Repeat();
							state387.setSname("ForceA1");
							if (!flowThread.callState(state387, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 674, 46)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 675
							flowThread.reentryState(this, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 675, 15)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 671, 61));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class ForceQ2 extends Dialog {

		final State currentState = this;

		public Object forceResponse;
		public Object repeat;

		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 682
			try {
				EXECUTION: {
					int count = getCount(801197928) + 1;
					incrCount(801197928);
					// Line: 683
					if (count == 1) {
						// Line: 684
						boolean chosen388 = false;
						boolean matching389 = true;
						while (!chosen388 && matching389) {
							int rand390 = random(1711574013, 3, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
							matching389 = false;
							if (true) {
								matching389 = true;
								if (rand390 >= 0 && rand390 < 1) {
									chosen388 = true;
									iristk.situated.SystemAgentFlow.say state391 = agent.new say();
									StringCreator string392 = new StringCreator();
									string392.append("The question:");
									state391.setText(string392.toString());
									if (!flowThread.callState(state391, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 684, 13)))) {
										eventResult = EVENT_ABORTED;
										break EXECUTION;
									}
								}
							}
							if (true) {
								matching389 = true;
								if (rand390 >= 1 && rand390 < 2) {
									chosen388 = true;
									iristk.situated.SystemAgentFlow.say state393 = agent.new say();
									StringCreator string394 = new StringCreator();
									string394.append("Here's the question:");
									state393.setText(string394.toString());
									if (!flowThread.callState(state393, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 684, 13)))) {
										eventResult = EVENT_ABORTED;
										break EXECUTION;
									}
								}
							}
							if (true) {
								matching389 = true;
								if (rand390 >= 2 && rand390 < 3) {
									chosen388 = true;
									iristk.situated.SystemAgentFlow.say state395 = agent.new say();
									StringCreator string396 = new StringCreator();
									string396.append("Okay, here it is:");
									state395.setText(string396.toString());
									if (!flowThread.callState(state395, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 684, 13)))) {
										eventResult = EVENT_ABORTED;
										break EXECUTION;
									}
								}
							}
						}
					}
					iristk.situated.SystemAgentFlow.say state397 = agent.new say();
					StringCreator string398 = new StringCreator();
					// Line: 684
					if (count==1) {
						string398.append("When a rifle is fired, the exploding gunpowder pushes  			forward on the bullet, and in accordance with newton's third law, an equal force is applied  			to the rifle, making it recoil backwards.");
					}
					string398.append("Which object accelerates faster, the fired bullet  			or the recoiling rifle?");
					state397.setText(string398.toString());
					if (!flowThread.callState(state397, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 682, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					iristk.situated.SystemAgentFlow.listen state399 = agent.new listen();
					if (!flowThread.callState(state399, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 682, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 682, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 696
			try {
				count = getCount(1631862159) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:force2")) {
						incrCount(1631862159);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 697
							forceResponse = event.get("sem:force2");
							// Line: 698
							if (eq(forceResponse,1)) {
								// Line: 699
								ForceE2 state400 = new ForceE2();
								state400.setResponse(1);
								flowThread.gotoState(state400, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 699, 43)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 701
							if (eq(forceResponse,2)) {
								// Line: 702
								ForceE2 state401 = new ForceE2();
								state401.setResponse(2);
								flowThread.gotoState(state401, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 702, 43)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 704
							if (eq(forceResponse,3)) {
								// Line: 705
								ForceE2 state402 = new ForceE2();
								state402.setResponse(3);
								flowThread.gotoState(state402, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 705, 43)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 707
							if (eq(forceResponse,4)) {
								iristk.situated.SystemAgentFlow.say state403 = agent.new say();
								StringCreator string404 = new StringCreator();
								string404.append("That's okay");
								state403.setText(string404.toString());
								if (!flowThread.callState(state403, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 707, 35)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
								// Line: 709
								ForceA2 state405 = new ForceA2();
								state405.setResponseA(4);
								flowThread.gotoState(state405, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 709, 44)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 696, 61));
			}
			// Line: 712
			try {
				count = getCount(2094777811) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:repeat")) {
						incrCount(2094777811);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 713
							repeat = event.get("sem:repeat");
							// Line: 714
							if (eq(repeat,1)) {
								iristk.situated.SystemAgentFlow.say state406 = agent.new say();
								StringCreator string407 = new StringCreator();
								string407.append("Of course!");
								state406.setText(string407.toString());
								if (!flowThread.callState(state406, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 714, 28)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
								// Line: 716
								ForceQ2 state408 = new ForceQ2();
								flowThread.gotoState(state408, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 716, 29)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							iristk.situated.SystemAgentFlow.say state409 = agent.new say();
							StringCreator string410 = new StringCreator();
							string410.append("Would you like me to repeat the full question?");
							state409.setText(string410.toString());
							if (!flowThread.callState(state409, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 712, 61)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 719
							Repeat state411 = new Repeat();
							state411.setSname("ForceQ2");
							if (!flowThread.callState(state411, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 719, 46)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 720
							flowThread.reentryState(this, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 720, 15)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 712, 61));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class ForceE2 extends Dialog {

		final State currentState = this;
		public Integer response = null;

		public void setResponse(Object value) {
			if (value != null) {
				response = asInteger(value);
				params.put("response", value);
			}
		}

		public Object repeat;

		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 727
			try {
				EXECUTION: {
					int count = getCount(984849465) + 1;
					incrCount(984849465);
					// Line: 728
					boolean chosen412 = false;
					boolean matching413 = true;
					while (!chosen412 && matching413) {
						int rand414 = random(787387795, 3, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
						matching413 = false;
						if (true) {
							matching413 = true;
							if (rand414 >= 0 && rand414 < 1) {
								chosen412 = true;
								iristk.situated.SystemAgentFlow.say state415 = agent.new say();
								StringCreator string416 = new StringCreator();
								string416.append("Why do you say that?");
								state415.setText(string416.toString());
								if (!flowThread.callState(state415, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 728, 12)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
							}
						}
						if (true) {
							matching413 = true;
							if (rand414 >= 1 && rand414 < 2) {
								chosen412 = true;
								iristk.situated.SystemAgentFlow.say state417 = agent.new say();
								StringCreator string418 = new StringCreator();
								string418.append("Why this choice?");
								state417.setText(string418.toString());
								if (!flowThread.callState(state417, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 728, 12)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
							}
						}
						if (true) {
							matching413 = true;
							if (rand414 >= 2 && rand414 < 3) {
								chosen412 = true;
								iristk.situated.SystemAgentFlow.say state419 = agent.new say();
								StringCreator string420 = new StringCreator();
								string420.append("Why this particular option?");
								state419.setText(string420.toString());
								if (!flowThread.callState(state419, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 728, 12)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
							}
						}
					}
					iristk.situated.SystemAgentFlow.listen state421 = agent.new listen();
					if (!flowThread.callState(state421, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 727, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 727, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 735
			try {
				count = getCount(2030562336) + 1;
				if (event.triggers("sense.user.speak")) {
					incrCount(2030562336);
					eventResult = EVENT_CONSUMED;
					EXECUTION: {
						// Line: 736
						boolean chosen422 = false;
						boolean matching423 = true;
						while (!chosen422 && matching423) {
							int rand424 = random(1416233903, 3, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
							matching423 = false;
							if (true) {
								matching423 = true;
								if (rand424 >= 0 && rand424 < 1) {
									chosen422 = true;
									iristk.situated.SystemAgentFlow.say state425 = agent.new say();
									StringCreator string426 = new StringCreator();
									string426.append("I see");
									state425.setText(string426.toString());
									if (!flowThread.callState(state425, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 736, 12)))) {
										eventResult = EVENT_ABORTED;
										break EXECUTION;
									}
								}
							}
							if (true) {
								matching423 = true;
								if (rand424 >= 1 && rand424 < 2) {
									chosen422 = true;
									iristk.situated.SystemAgentFlow.say state427 = agent.new say();
									StringCreator string428 = new StringCreator();
									string428.append("Interesting");
									state427.setText(string428.toString());
									if (!flowThread.callState(state427, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 736, 12)))) {
										eventResult = EVENT_ABORTED;
										break EXECUTION;
									}
								}
							}
							if (true) {
								matching423 = true;
								if (rand424 >= 2 && rand424 < 3) {
									chosen422 = true;
									iristk.situated.SystemAgentFlow.say state429 = agent.new say();
									StringCreator string430 = new StringCreator();
									string430.append("Ah");
									state429.setText(string430.toString());
									if (!flowThread.callState(state429, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 736, 12)))) {
										eventResult = EVENT_ABORTED;
										break EXECUTION;
									}
								}
							}
						}
						// Line: 741
						ForceA2 state431 = new ForceA2();
						state431.setResponseA(response);
						flowThread.gotoState(state431, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 741, 51)));
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					if (eventResult != EVENT_IGNORED) return eventResult;
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 735, 36));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class ForceA2 extends Dialog {

		final State currentState = this;
		public Integer responseA = null;

		public void setResponseA(Object value) {
			if (value != null) {
				responseA = asInteger(value);
				params.put("responseA", value);
			}
		}

		public Object repeat;

		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 748
			try {
				EXECUTION: {
					int count = getCount(1748225580) + 1;
					incrCount(1748225580);
					// Line: 749
					if (responseA==1 && count==1) {
						iristk.situated.SystemAgentFlow.say state432 = agent.new say();
						StringCreator string433 = new StringCreator();
						string433.append("You're correct that the bullet will accelerate faster.");
						state432.setText(string433.toString());
						if (!flowThread.callState(state432, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 749, 41)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					// Line: 752
					if (responseA==2 && count==1) {
						iristk.situated.SystemAgentFlow.say state434 = agent.new say();
						StringCreator string435 = new StringCreator();
						string435.append("The rifle is a curious choice. It has the higher mass, so it isn't  				unreasonable to think it might have the higher acceleration.");
						state434.setText(string435.toString());
						if (!flowThread.callState(state434, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 752, 41)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					// Line: 756
					if (responseA==3 && count==1) {
						iristk.situated.SystemAgentFlow.say state436 = agent.new say();
						StringCreator string437 = new StringCreator();
						string437.append("The idea the two accelerate at the same rate is common, because of  				course as we've explained, the force on both is equal.");
						state436.setText(string437.toString());
						if (!flowThread.callState(state436, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 756, 41)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					// Line: 760
					if (responseA==4 && count==1) {
						iristk.situated.SystemAgentFlow.say state438 = agent.new say();
						StringCreator string439 = new StringCreator();
						string439.append("Hopefully this will help you understand.");
						state438.setText(string439.toString());
						if (!flowThread.callState(state438, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 760, 41)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					// Line: 763
					if (count==1) {
						iristk.situated.SystemAgentFlow.say state440 = agent.new say();
						StringCreator string441 = new StringCreator();
						string441.append("The");
						// Line: 763
						if (responseA==2 || responseA==3) {
							string441.append("correct");
						}
						string441.append("answer");
						// Line: 763
						if (responseA==2 || responseA==3) {
							string441.append(", however,");
						}
						string441.append("is that the  				bullet will accelerate faster, much faster, than the rifle. To see this we  				should remember newton's second law, explained in the free fall section:  				force equals mass times acceleration, or rewritten, acceleration equals  				force divided by mass. So, since we know that the force is equal on both  				the rifle and bullet, but that the bullet is far smaller, we can see the bullet  				must accelerate much faster. Interestingly, this also means that lighter guns have a  				much faster recoil using the same ammunition, because the lighter the gun, the faster  				it accelerates backwards. Target shooters actually prefer heavier guns for this  				reason.");
						state440.setText(string441.toString());
						if (!flowThread.callState(state440, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 763, 24)))) {
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
					}
					iristk.situated.SystemAgentFlow.say state442 = agent.new say();
					StringCreator string443 = new StringCreator();
					string443.append("That's it on this topic, would you like to go back to the topic list?");
					state442.setText(string443.toString());
					if (!flowThread.callState(state442, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 748, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					iristk.situated.SystemAgentFlow.listen state444 = agent.new listen();
					if (!flowThread.callState(state444, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 748, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 748, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 779
			try {
				count = getCount(1688019098) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:yes")) {
						incrCount(1688019098);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 780
							boolean chosen445 = false;
							boolean matching446 = true;
							while (!chosen445 && matching446) {
								int rand447 = random(1792845110, 3, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
								matching446 = false;
								if (true) {
									matching446 = true;
									if (rand447 >= 0 && rand447 < 1) {
										chosen445 = true;
										iristk.situated.SystemAgentFlow.say state448 = agent.new say();
										StringCreator string449 = new StringCreator();
										string449.append("Okay! Back to the topics!");
										state448.setText(string449.toString());
										if (!flowThread.callState(state448, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 780, 12)))) {
											eventResult = EVENT_ABORTED;
											break EXECUTION;
										}
									}
								}
								if (true) {
									matching446 = true;
									if (rand447 >= 1 && rand447 < 2) {
										chosen445 = true;
										iristk.situated.SystemAgentFlow.say state450 = agent.new say();
										StringCreator string451 = new StringCreator();
										string451.append("Nice, to the topics for more questions");
										state450.setText(string451.toString());
										if (!flowThread.callState(state450, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 780, 12)))) {
											eventResult = EVENT_ABORTED;
											break EXECUTION;
										}
									}
								}
								if (true) {
									matching446 = true;
									if (rand447 >= 2 && rand447 < 3) {
										chosen445 = true;
										iristk.situated.SystemAgentFlow.say state452 = agent.new say();
										StringCreator string453 = new StringCreator();
										string453.append("Great! More topics it is.");
										state452.setText(string453.toString());
										if (!flowThread.callState(state452, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 780, 12)))) {
											eventResult = EVENT_ABORTED;
											break EXECUTION;
										}
									}
								}
							}
							// Line: 785
							TopicList state454 = new TopicList();
							flowThread.gotoState(state454, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 785, 30)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 779, 58));
			}
			// Line: 787
			try {
				count = getCount(687241927) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:no")) {
						incrCount(687241927);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							iristk.situated.SystemAgentFlow.say state455 = agent.new say();
							StringCreator string456 = new StringCreator();
							string456.append("Okay.");
							state455.setText(string456.toString());
							if (!flowThread.callState(state455, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 787, 57)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 789
							ExitDialog state457 = new ExitDialog();
							if (!flowThread.callState(state457, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 789, 31)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 790
							flowThread.reentryState(this, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 790, 15)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 787, 57));
			}
			// Line: 792
			try {
				count = getCount(226170135) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:repeat")) {
						incrCount(226170135);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 793
							repeat = event.get("sem:repeat");
							iristk.situated.SystemAgentFlow.say state458 = agent.new say();
							StringCreator string459 = new StringCreator();
							string459.append("Would you like me to repeat the full answer?");
							state458.setText(string459.toString());
							if (!flowThread.callState(state458, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 792, 61)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 795
							Repeat state460 = new Repeat();
							state460.setSname("ForceA2");
							if (!flowThread.callState(state460, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 795, 46)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 796
							flowThread.reentryState(this, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 796, 15)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 792, 61));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class TopicOutro extends Dialog {

		final State currentState = this;


		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 801
			try {
				EXECUTION: {
					int count = getCount(245672235) + 1;
					incrCount(245672235);
					iristk.situated.SystemAgentFlow.say state461 = agent.new say();
					StringCreator string462 = new StringCreator();
					string462.append("Would you like to go back to the topic list?");
					state461.setText(string462.toString());
					if (!flowThread.callState(state461, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 801, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					iristk.situated.SystemAgentFlow.listen state463 = agent.new listen();
					if (!flowThread.callState(state463, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 801, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 801, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 805
			try {
				count = getCount(1012570586) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:yes")) {
						incrCount(1012570586);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 806
							boolean chosen464 = false;
							boolean matching465 = true;
							while (!chosen464 && matching465) {
								int rand466 = random(1207140081, 3, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
								matching465 = false;
								if (true) {
									matching465 = true;
									if (rand466 >= 0 && rand466 < 1) {
										chosen464 = true;
										iristk.situated.SystemAgentFlow.say state467 = agent.new say();
										StringCreator string468 = new StringCreator();
										string468.append("Okay!");
										state467.setText(string468.toString());
										if (!flowThread.callState(state467, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 806, 12)))) {
											eventResult = EVENT_ABORTED;
											break EXECUTION;
										}
									}
								}
								if (true) {
									matching465 = true;
									if (rand466 >= 1 && rand466 < 2) {
										chosen464 = true;
										iristk.situated.SystemAgentFlow.say state469 = agent.new say();
										StringCreator string470 = new StringCreator();
										string470.append("All right!");
										state469.setText(string470.toString());
										if (!flowThread.callState(state469, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 806, 12)))) {
											eventResult = EVENT_ABORTED;
											break EXECUTION;
										}
									}
								}
								if (true) {
									matching465 = true;
									if (rand466 >= 2 && rand466 < 3) {
										chosen464 = true;
										iristk.situated.SystemAgentFlow.say state471 = agent.new say();
										StringCreator string472 = new StringCreator();
										string472.append("Great!");
										state471.setText(string472.toString());
										if (!flowThread.callState(state471, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 806, 12)))) {
											eventResult = EVENT_ABORTED;
											break EXECUTION;
										}
									}
								}
							}
							// Line: 811
							TopicList state473 = new TopicList();
							flowThread.gotoState(state473, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 811, 30)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 805, 58));
			}
			// Line: 813
			try {
				count = getCount(1908923184) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:no")) {
						incrCount(1908923184);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 814
							ExitDialog state474 = new ExitDialog();
							if (!flowThread.callState(state474, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 814, 31)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 815
							flowThread.reentryState(this, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 815, 15)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 813, 57));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class Repeat extends State {

		final State currentState = this;
		public String sname = null;

		public void setSname(Object value) {
			if (value != null) {
				sname = asString(value);
				params.put("sname", value);
			}
		}


		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 821
			try {
				EXECUTION: {
					int count = getCount(532445947) + 1;
					incrCount(532445947);
					iristk.situated.SystemAgentFlow.listen state475 = agent.new listen();
					if (!flowThread.callState(state475, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 821, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 821, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 824
			try {
				count = getCount(1170794006) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:yes")) {
						incrCount(1170794006);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							// Line: 825
							boolean chosen476 = false;
							boolean matching477 = true;
							while (!chosen476 && matching477) {
								int rand478 = random(1289479439, 3, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
								matching477 = false;
								if (true) {
									matching477 = true;
									if (rand478 >= 0 && rand478 < 1) {
										chosen476 = true;
										iristk.situated.SystemAgentFlow.say state479 = agent.new say();
										StringCreator string480 = new StringCreator();
										string480.append("Okay!");
										state479.setText(string480.toString());
										if (!flowThread.callState(state479, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 825, 12)))) {
											eventResult = EVENT_ABORTED;
											break EXECUTION;
										}
									}
								}
								if (true) {
									matching477 = true;
									if (rand478 >= 1 && rand478 < 2) {
										chosen476 = true;
										iristk.situated.SystemAgentFlow.say state481 = agent.new say();
										StringCreator string482 = new StringCreator();
										string482.append("All right!");
										state481.setText(string482.toString());
										if (!flowThread.callState(state481, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 825, 12)))) {
											eventResult = EVENT_ABORTED;
											break EXECUTION;
										}
									}
								}
								if (true) {
									matching477 = true;
									if (rand478 >= 2 && rand478 < 3) {
										chosen476 = true;
										iristk.situated.SystemAgentFlow.say state483 = agent.new say();
										StringCreator string484 = new StringCreator();
										string484.append("Great!");
										state483.setText(string484.toString());
										if (!flowThread.callState(state483, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 825, 12)))) {
											eventResult = EVENT_ABORTED;
											break EXECUTION;
										}
									}
								}
							}
							// Line: 830
							if (eq(sname,"TopicList")) {
								// Line: 831
								TopicList state485 = new TopicList();
								flowThread.gotoState(state485, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 831, 31)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 833
							if (eq(sname,"FreeFallIntro")) {
								// Line: 834
								FreeFallIntro state486 = new FreeFallIntro();
								flowThread.gotoState(state486, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 834, 35)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 836
							if (eq(sname,"FreeFallQ1")) {
								// Line: 837
								FreeFallQ1 state487 = new FreeFallQ1();
								flowThread.gotoState(state487, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 837, 32)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 839
							if (eq(sname,"FreeFallA1")) {
								// Line: 840
								FreeFallA1 state488 = new FreeFallA1();
								flowThread.gotoState(state488, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 840, 32)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 842
							if (eq(sname,"FreeFallExtra")) {
								// Line: 843
								FreeFallExtra state489 = new FreeFallExtra();
								flowThread.gotoState(state489, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 843, 35)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 845
							if (eq(sname,"GravityIntro")) {
								// Line: 846
								GravityIntro state490 = new GravityIntro();
								flowThread.gotoState(state490, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 846, 34)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 848
							if (eq(sname,"GravityQ1")) {
								// Line: 849
								GravityQ1 state491 = new GravityQ1();
								flowThread.gotoState(state491, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 849, 31)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 851
							if (eq(sname,"GravityA1")) {
								// Line: 852
								GravityA1 state492 = new GravityA1();
								flowThread.gotoState(state492, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 852, 31)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 854
							if (eq(sname,"GravityExtra")) {
								// Line: 855
								GravityExtra state493 = new GravityExtra();
								flowThread.gotoState(state493, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 855, 34)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 857
							if (eq(sname,"ForceIntro")) {
								// Line: 858
								ForceIntro state494 = new ForceIntro();
								flowThread.gotoState(state494, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 858, 32)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 860
							if (eq(sname,"ForceQ1")) {
								// Line: 861
								ForceQ1 state495 = new ForceQ1();
								flowThread.gotoState(state495, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 861, 29)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 863
							if (eq(sname,"ForceA1")) {
								// Line: 864
								ForceA1 state496 = new ForceA1();
								flowThread.gotoState(state496, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 864, 29)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 866
							if (eq(sname,"ForceQ2")) {
								// Line: 867
								ForceQ2 state497 = new ForceQ2();
								flowThread.gotoState(state497, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 867, 29)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 869
							if (eq(sname,"ForceA2")) {
								// Line: 870
								ForceA2 state498 = new ForceA2();
								flowThread.gotoState(state498, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 870, 29)));
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 824, 58));
			}
			// Line: 873
			try {
				count = getCount(1277181601) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:no")) {
						incrCount(1277181601);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							iristk.situated.SystemAgentFlow.say state499 = agent.new say();
							StringCreator string500 = new StringCreator();
							string500.append("Okay!");
							state499.setText(string500.toString());
							if (!flowThread.callState(state499, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 873, 57)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							iristk.situated.SystemAgentFlow.say state501 = agent.new say();
							StringCreator string502 = new StringCreator();
							string502.append("All right!");
							state501.setText(string502.toString());
							if (!flowThread.callState(state501, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 873, 57)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 876
							flowThread.returnFromCall(this, null, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 876, 14)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 873, 57));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class Goodbye extends State {

		final State currentState = this;


		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 881
			try {
				EXECUTION: {
					int count = getCount(1209271652) + 1;
					incrCount(1209271652);
					// Line: 882
					boolean chosen503 = false;
					boolean matching504 = true;
					while (!chosen503 && matching504) {
						int rand505 = random(93122545, 2, iristk.util.RandomList.RandomModel.DECK_RESHUFFLE_NOREPEAT);
						matching504 = false;
						if (true) {
							matching504 = true;
							if (rand505 >= 0 && rand505 < 1) {
								chosen503 = true;
								iristk.situated.SystemAgentFlow.say state506 = agent.new say();
								StringCreator string507 = new StringCreator();
								string507.append("See you later!");
								state506.setText(string507.toString());
								if (!flowThread.callState(state506, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 882, 12)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
							}
						}
						if (true) {
							matching504 = true;
							if (rand505 >= 1 && rand505 < 2) {
								chosen503 = true;
								iristk.situated.SystemAgentFlow.say state508 = agent.new say();
								StringCreator string509 = new StringCreator();
								string509.append("Goodbye!");
								state508.setText(string509.toString());
								if (!flowThread.callState(state508, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 882, 12)))) {
									eventResult = EVENT_ABORTED;
									break EXECUTION;
								}
							}
						}
					}
					// Line: 886
					Idle state510 = new Idle();
					flowThread.gotoState(state510, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 886, 24)));
					eventResult = EVENT_ABORTED;
					break EXECUTION;
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 881, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


	private class ExitDialog extends Dialog {

		final State currentState = this;


		@Override
		public void setFlowThread(FlowRunner.FlowThread flowThread) {
			super.setFlowThread(flowThread);
		}

		@Override
		public void onentry() throws Exception {
			int eventResult;
			Event event = new Event("state.enter");
			// Line: 891
			try {
				EXECUTION: {
					int count = getCount(557041912) + 1;
					incrCount(557041912);
					iristk.situated.SystemAgentFlow.say state511 = agent.new say();
					StringCreator string512 = new StringCreator();
					string512.append("Are you sure?");
					state511.setText(string512.toString());
					if (!flowThread.callState(state511, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 891, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
					iristk.situated.SystemAgentFlow.listen state513 = agent.new listen();
					if (!flowThread.callState(state513, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 891, 12)))) {
						eventResult = EVENT_ABORTED;
						break EXECUTION;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 891, 12));
			}
		}

		@Override
		public int onFlowEvent(Event event) throws Exception {
			int eventResult;
			int count;
			// Line: 895
			try {
				count = getCount(1134712904) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:yes")) {
						incrCount(1134712904);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							iristk.situated.SystemAgentFlow.say state514 = agent.new say();
							StringCreator string515 = new StringCreator();
							string515.append("Okay.");
							state514.setText(string515.toString());
							if (!flowThread.callState(state514, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 895, 58)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 897
							Goodbye state516 = new Goodbye();
							flowThread.gotoState(state516, currentState, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 897, 28)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 895, 58));
			}
			// Line: 899
			try {
				count = getCount(1435804085) + 1;
				if (event.triggers("sense.user.speak")) {
					if (event.has("sem:no")) {
						incrCount(1435804085);
						eventResult = EVENT_CONSUMED;
						EXECUTION: {
							iristk.situated.SystemAgentFlow.say state517 = agent.new say();
							StringCreator string518 = new StringCreator();
							string518.append("Okay. Back we go then!");
							state517.setText(string518.toString());
							if (!flowThread.callState(state517, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 899, 57)))) {
								eventResult = EVENT_ABORTED;
								break EXECUTION;
							}
							// Line: 901
							flowThread.returnFromCall(this, null, new FlowEventInfo(currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 901, 14)));
							eventResult = EVENT_ABORTED;
							break EXECUTION;
						}
						if (eventResult != EVENT_IGNORED) return eventResult;
					}
				}
			} catch (Exception e) {
				throw new FlowException(e, currentState, event, new XMLLocation(new File("D:\\ProgramProjects\\FurhatPhysicsTutor\\IrisTK\\app\\grace\\src\\iristk\\app\\grace\\GraceFlow.xml"), 899, 57));
			}
			eventResult = super.onFlowEvent(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			eventResult = callerHandlers(event);
			if (eventResult != EVENT_IGNORED) return eventResult;
			return EVENT_IGNORED;
		}

	}


}
